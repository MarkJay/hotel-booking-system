<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaciltyRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilty_room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('facilty_id');
            $table->foreign('facilty_id')
                ->on('facilties')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->unsignedBigInteger('room_id');
            $table->foreign('room_id')
                ->on('rooms')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilty_room');
    }
}
