<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->delete();

        DB::table('statuses')->insert(array(
            0 => array(
                'name' => 'Pending',
                'created_at' => now(),
                'updated_at' => now()
            ),
            1 => array(
                'name' => 'Approved',
                'created_at' => now(),
                'updated_at' => now()
            ),
            2 => array(
                'name' => 'Cancelled',
                'created_at' => now(),
                'updated_at' => now()
            )
            
        ));
    }
}
