<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        DB::table('roles')->insert(array(
            0 => array(
                'name' => 'Users',
                'created_at' => now(),
                'updated_at' => now()
            ),
            1 => array(
                'name' => 'Admin',
                'created_at' => now(),
                'update_at' => now()
            )
            ));
    }
}
