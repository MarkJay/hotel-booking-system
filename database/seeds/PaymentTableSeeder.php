<?php

use Illuminate\Database\Seeder;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->delete();

        DB::table('payments')->insert(array(
            0 => array (
                'name' => 'over the counter',
                'created_at' => now(),
                'updated_at' => now()
            ),
            1 => array (
                'name' => 'paypal',
                'created_at' => now(),
                'updated_at' => now()
            ),
            2 => array (
                'name' => 'pending',
                'created_at' => now(),
                'updated_at' => now()
            )
           
        ));
    }
}
