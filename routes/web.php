<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookingController@userHome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/findCategoryName', 'BookingController@findCategoryName');

//User View

Route::get('/user-home', 'BookingController@userHome')->name('user-home');



Route::middleware("admin")->group(function(){

//Categories CRUD

Route::get('/categories', 'CategoryController@index');
Route::get('/add-categories', 'CategoryController@create');
Route::post('/add-categories', 'CategoryController@store');
Route::delete('/delete-categories', 'CategoryController@destroy');
Route::get('/update-categories/{id}', 'CategoryController@edit');
Route::post('/update-categories/{id}', 'CategoryController@update');

//Facilities CRUD

Route::get('/facilities', 'FaciltyController@index');
Route::get('/add-facilities', 'FaciltyController@create');
Route::post('/add-facilities', 'FaciltyController@store');
Route::delete('/delete-facilities', 'FaciltyController@destroy');
Route::get('/update-facilities/{id}', 'FaciltyController@edit');
Route::post('/update-facilities/{id}', 'FaciltyController@update');
Route::get('/add-room-facilities', 'FaciltyController@addToRoomFacility');
Route::post('/add-room-facilities', 'FaciltyController@createRoomFacility');
Route::get('/search-room', 'FaciltyController@searchRoom');
Route::post('/remove-facility', 'FaciltyController@removeFacilityRoom');
Route::get('/findRoomName', 'FaciltyController@findRoomName');

//Rooms CRUD
Route::get('/rooms', 'RoomController@index');
Route::get('/add-rooms', 'RoomController@create');
Route::post('/add-rooms', 'RoomController@store');
Route::delete('/delete-rooms', 'RoomController@destroy');
Route::get('/update-rooms/{id}', 'RoomController@edit');
Route::patch('/update-rooms/{id}', 'RoomController@update');


//Bookings

Route::get('/all-bookings', 'BookingController@showAllBooking');
Route::get('/show-booking-details/{id}', 'BookingController@showBookingDetails');
Route::get('/new-bookings', 'BookingController@showNewBooking');
Route::get('/cancelled-bookings', 'BookingController@showCancelledBooking');
Route::get('/approved-bookings', 'BookingController@showApprovedBooking');
Route::post('/update-booking', 'BookingController@updateBooking');
Route::get('/show-all-booking-details/{id}', 'BookingController@showAllbookingDetails');
Route::get('/search', 'BookingController@search');

// Users
Route::get('/all-user-registered', 'UserController@show');
Route::get('/admin', 'UserController@showAdmin');
Route::get('/admin-user', 'UserController@create');
Route::post('/admin-user', 'UserController@store');
Route::delete('/delete-admin', 'UserController@destroy');
Route::delete('/delete-users', 'UserController@deleteUser');

//Reports
Route::get('/reports', 'BookingController@reports');
Route::post('/reports', 'BookingController@generateReports');
Route::get('/download-report-csv', 'AddonController@downloadReportsCSV');

//Messages Admin

Route::get('/messages', 'ContactController@showAllMessages');
Route::get('/show-message/{id}', 'ContactController@showMessage');
Route::delete('/delete-message', 'ContactController@deleteMessage');

//About page

Route::get('/about-us-page', 'AboutController@aboutUs');
Route::get('/create-about-us-page', 'AboutController@create');
Route::post('/create-about-us-page', 'AboutController@store');
Route::delete('/delete-about-us-page', 'AboutController@destroy');
Route::get('/update-about-us-page/{id}', 'AboutController@edit');
Route::patch('/update-about-us-page/{id}', 'AboutController@update');

});



Route::middleware("customer")->group(function(){

    // User Profile

Route::get('/user-profile', 'ProfileController@index');
Route::get('/create-profile', 'ProfileController@create');
Route::post('/create-profile', 'ProfileController@store');
Route::delete('/delete-profile', 'ProfileController@destroy');
Route::get('/update-profile/{id}', 'ProfileController@edit');
Route::patch('/update-profile/{id}', 'ProfileController@update');


//Booking of user

Route::get('/user-booking-details/{id}', 'BookingController@showUserBookingDetails');
Route::delete('/cancel-booking', 'BookingController@cancelBooking');
Route::post('/check-out', 'BookingController@checkout');

});

// Show Room to User

Route::get('/show-rooms', 'RoomController@showRooms'); 
Route::get('/show-selected-room/{id}', 'RoomController@showSelectedRoom');


//Show Booking of User

Route::get('/show-users-booking', 'BookingController@showUserBooking');
Route::post('/book-room', 'BookingController@bookRoom');
Route::post('/check-availability', 'BookingController@checkRoomAvailability');



//About Us
Route::get('/about-us', 'AboutController@aboutUsUser');
Route::get('/contact-us', 'ContactController@contactUs');



//Send Message

Route::post('/send-message', 'ContactController@sendMessage');