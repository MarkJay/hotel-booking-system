@extends('layouts.templates.template')
@section('title', 'All Admin')
@section('content')

<div id="container">

    <div class="card card-cascade narrower mt-5">
        <!--Card image-->
        <div
          class="view view-cascade gradient-card-header young-passion-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-center align-items-center">
          <h3 class="white-text mx-3 p-3 text-uppercase">Admin </h3>  
        </div>
        <!--/Card image-->
        
        <div class="px-4">
            <div class="gradient-card-header">{{ $users->links() }}</div>
          <div class="table-wrapper">
            <!--Table-->
            <table class="table table-hover mb-1">
      
              <!--Table head-->
              <thead class="text-uppercase">
                <tr>
                  <th class="th-lg font-weight-bold">
                    Admin Firstname
                  </th>
                  <th class="th-lg font-weight-bold">
                    <a href="">Admin Lastname
                  </th>
                  <th class="th-lg font-weight-bold ">
                    <a href="">Email
                      
                    </a>
                  </th>
                  <th class="th-lg font-weight-bold ">
                    <a href="">Date Created
                      
                    </a>
                  </th>
                  <th class="th-lg font-weight-bold">
                    <a href="">Action
                      
                    </a>
                </tr>
              </thead>
              <!--Table head-->
      
              <!--Table body-->
              <tbody>
                @foreach ($users as $user)
                <tr>
                  <td>{{ $user->firstname }}</td>
                  <td>{{ $user->lastname }}</span></td>
                  <td>{{ $user->email }}</span></td>
                
                  <td><span class="bg-elegant">{{ $user->created_at }}</span></td>
                  <td>
                    <form action="/delete-admin" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="admin_id" value="{{ $user->id }}">
                        <button type="submit" class="btn btn-elegant" onclick="return confirm('Are you sure you want to delete this item?')"><i class="fas fa-trash"></i> Delete</button>
                    {{-- <a href="/update-categories/{{ $category->id }}" class="btn btn-ripe-malinka-gradient"><i class="fas fa-edit"></i> Update</a> --}}
                    </form>
                </td>
                </tr>
            @endforeach
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
          </div>
      
        </div>
      
      </div>
      <!-- Table with panel -->
    </div>  

@endsection