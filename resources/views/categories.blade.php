@extends('layouts.templates.template')
@section('title', 'All Categories')
@section('content')

<div id="container">

<div class="card card-cascade narrower mt-5">
    <!--Card image-->
    <div
      class="view view-cascade gradient-card-header young-passion-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-center align-items-center">
      <h3 class="white-text mx-3 p-3 text-uppercase">Room Categories </h3>  
    </div>
    <!--/Card image-->
    
    <div class="px-4">
        <div class="gradient-card-header">{{ $categories->links() }}</div>
      <div class="table-wrapper">
        <!--Table-->
        <table class="table table-hover mb-1">
  
          <!--Table head-->
          <thead class="text-uppercase">
            <tr>
              <th class="th-lg font-weight-bold">
                Category Name
              </th>
              <th class="th-lg font-weight-bold">
                <a href="">Date Created
                </a>
              </th>
              <th class="th-lg font-weight-bold ">
                <a href="">Date Updated
                  
                </a>
              </th>
              <th class="th-lg font-weight-bold">
                <a href="">Action
                  
                </a>
            </tr>
          </thead>
          <!--Table head-->
  
          <!--Table body-->
          <tbody class="text-center">
            @foreach ($categories as $category)
            <tr>
              <td>{{ $category->name }}</td>
              <td>{{ $category->created_at }}</span></td>
            
              <td><span class="bg-elegant">{{ $category->updated_at }}</span></td>
              <td>
                <form action="/delete-categories" method="post">
                @csrf
                @method('DELETE')
                <input type="hidden" name="category_id" value="{{ $category->id }}">
                    <button type="submit" class="btn btn-elegant" onclick="return confirm('Are you sure you want to delete this item?')"><i class="fas fa-trash"></i> Delete</button>
                <a href="/update-categories/{{ $category->id }}" class="btn btn-ripe-malinka-gradient"><i class="fas fa-edit"></i> Update</a>
                </form>
            </td>
            </tr>
        @endforeach
          </tbody>
          <!--Table body-->
        </table>
        <!--Table-->
      </div>
  
    </div>
  
  </div>
  <!-- Table with panel -->
</div>  
   <script>
    //    const deleteBtns = document.querySelectorAll('.deleteBtn');

    //    deleteBtns.forEach(function(deleteBtn){
    //         deleteBtn.addEventListener('click', function(){
    //             console.log('this is deleted');
    //         })
    //    })



   </script>
@endsection