@extends('layouts.templates.template')
@section('title', 'Manage Rooms')
@section('content')

<div id="container">
    <div class="card card-cascade narrower mt-5">
        <!--Card image-->
        <div
          class="view view-cascade gradient-card-header young-passion-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-center align-items-center">
          <h3 class="white-text mx-3 p-3 text-uppercase">Manage Rooms</h3>  
        </div>
        <!--/Card image-->
        <div class="px-4">
            <div class="gradient-card-header p-0">{{ $rooms->links() }}</div>
          <div class="table-wrapper">
            <!--Table-->
            <table class="table table-hover mb-1">
      
              <!--Table head-->
              <thead class="text-uppercase text-center">
                <tr>
                  <th class="th-lg font-weight-bold">Room Name</th>
                  <th class="th-lg font-weight-bold">Room Description</th>
                  {{-- <th class="th-lg font-weight-bold ">Bed Number</th>
                  <th class="th-lg font-weight-bold">Capacity</th> --}}
                  <th class="th-lg font-weight-bold">Quantity</th> 
                  <th class="th-lg font-weight-bold">Price</th>
                  <th class="th-lg font-weight-bold">Image</th>
                  <th class="th-lg font-weight-bold">Date Created</th>
                  <th class="th-lg font-weight-bold">Action</th>
                </tr>
              </thead>
              <!--Table head-->
      
              <!--Table body-->
              <tbody class="text-center">
                @foreach ($rooms as $room)
                <tr>
                  <td>{{ $room->category->name }}</td>
                  <td class="roomDescription">{{ $room->description }}</td>
                    {{-- <td>{{ $room->bed_room }}</td>
                    <td>{{ $room->capacity }}</td> --}}
                    <td>{{ $room->quantity }}</td> 
                    <td>{{ $room->price }}</td>
                
                    <td>
                    <img src="{{ asset($room->imgPath) }}" alt="" height="100px" width="100px">
                    </td>
                    <td><span class="p-1 bg-info text-white rounded">{{ $room->created_at }}</span></td>
                    <td>
                <form action="/delete-rooms" method="post">
                @csrf
                @method('DELETE')
                @foreach ($room->facility as $facility)
                  @php
                    
                    // foreach ($facility as $key => $value) {
                    
                        
                    // }
                    //   echo $lists;
                  @endphp
                {{-- <input type="text" name="facility_id" value=""> --}}
                @endforeach
                     <input type="hidden" name="room_id" value="{{ $room->id }}">
                    <button type="submit" class="btn btn-elegant deleteBtn" onclick="return confirm('Are you sure you want to delete this item?')"><i class="fas fa-trash"></i> Delete</button>
                <a href="/update-rooms/{{ $room->id }}" class="btn btn-ripe-malinka-gradient"><i class="fas fa-edit"></i>Update</a>
            </form>          
                </td>
                </tr>
            @endforeach
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
          </div>
      
        </div>
      
      </div>
      <!-- Table with panel -->
</div>
@endsection