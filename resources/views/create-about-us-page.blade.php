@extends('layouts.templates.template')
@section('title', 'About Page')
@section('content')

    <h1 class="text-center mt-5">About Page</h1>

        <form action="/create-about-us-page" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
        </div>
            <div class="form-group">
                <label for="">image</label>
                <input type="file" name="imgPath" id="" class="form-control">
            </div>

            <button class="btn btn-unique">Submit</button>
        </form>


@endsection