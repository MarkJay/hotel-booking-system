@extends('layouts.templates.user-template')
@section('title', 'My Profile')
@section('content')



<div class="container">
    <h1>My Profile</h1>

   @if (count($profiles) === 1)
  
   <div class="profile-container">
            @foreach ($profiles as $profile)
        
        <h3>Profile Information</h3>
        <div class="profile-wrapper">
            <div class="user-contact-information">
                <p><strong>EMAIL: </strong>{{ $profile->user->email }}</p>
                <hr>
                <p><strong>ADDRESS: </strong>{{ $profile->address }}</p>
                <hr>
                <p><strong>NUMBER: </strong>{{ $profile->number }}</p>
                <hr>
            </div>
            <div class="profile-img-name">
                <p> <img src="{{ asset($profile->imgPath) }}" alt="" width="250px" height="250px"></p>
                <p class="user-name"><strong>{{ $profile->user->firstname }} {{ $profile->user->lastname }}</strong></p>
                <p class="user-id">id: </strong>{{ $profile->user->created_at->format('U') }}</p>
            
            </div>
        </div>
        <p>
            <form action="/delete-profile" method="post">
                @csrf
                @method('DELETE')
            <input type="hidden" name="profile_id" id="" value="{{ $profile->id }}">
            <button type="submit" class="moreDetailsBtn">Delete</button>
            <a href="/update-profile/{{ $profile->id }}" class="moreDetailsBtn">Update</a>
            </form>

        </p>
            @endforeach   
    
   </div>
   @else
   <div class="profile-button-initiate">
   <a href="/create-profile" class="moreDetailsBtn">Please create your profile</a>
   </div>

   @endif
   


</div>
@endsection