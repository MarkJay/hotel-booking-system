<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table class="table table-striped border">
        <thead>
            <tr>
                <th>Booking Id</th>
                <th>Booking Date</th>
                <th>Booking User</th>
                <th>Booking Room</th>
                <th>Total</th>
                <th>Payment</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($bookings as $booking)
            <tr>
            <td>{{ $booking->created_at->format('U') }}</td>
            <td>{{ $booking->created_at }}</td>
            <td>{{ $booking->user->firstname }} {{ $booking->user->lastname }}</td>
            <td>{{ $booking->room->category->name }}</td>
            <td>{{ $booking->total }}</td>
            <td>{{ $booking->payment->name }}</td>
            <td>{{ $booking->status->name }}</td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</body>
</html>