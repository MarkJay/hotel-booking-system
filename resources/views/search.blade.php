@extends('layouts.templates.template')
@section('title', 'Search Booking')
@section('content')

    <h1 class="text-center mt-5">Search Bookings</h1>

<form action="/search-booking" method="GET" class="mt-5">
    @csrf
    <div class="col-lg-4 form-group">
    <label for="">Booking Number</label>
    <input type="text" name="search_booking" class="form-control mt-1" required="required">
    <button type="submit" class="btn btn-elegant">Search</button>
</div>
</form>
    


@endsection