@extends('layouts.templates.template')
@section('title', 'Add Room Categories')
@section('content')
<a href="/facilities" class="btn btn-info mt-5"><i class="fas fa-home"></i> Back</a>
<div id="container">

    <h1 class="text-center mt-5 mb-5">Add Facilities</h1>
    {{-- <form action="/add-facilities" method="post">
        @csrf --}}
        <div class="form-group">
        <label for="">Name:</label>
        <input type="text" name="facility_name" class="form-control" id="facility">
        <span class="text-danger"></span>
        </div>
        
        <button type="submit" class="btn btn-unique">Submit</button>
    {{-- </form> --}}
    <p class="" id="message"></p>
</div>
    <script>
        const facilityInput = document.getElementById('facility');
        const submitBtn = facilityInput.parentElement.nextElementSibling;

        submitBtn.disabled = true;

        function isEmpty(value, element){
            facilityInputValue = facilityInput.value;
            if(facilityInputValue === ""){
                element.nextElementSibling.textContent = "Please input facility name";
            }else {
                element.nextElementSibling.textContent = "";
            }

        }
        function enableButton(){
            if(facilityInput.value === ""){
                submitBtn.disabled = true;
                
            }else {
                submitBtn.disabled = false;
            }
        }

        facilityInput.addEventListener('blur', function(){
            facilityInputValue = facilityInput.value;

            isEmpty(facilityInputValue, facilityInput);
            enableButton();
        });

        submitBtn.addEventListener('click', function(){
            const facilityValue = facilityInput.value;

            let data = new FormData;

            data.append('_token','{{csrf_token() }}')
            data.append('facility_name', facilityInput.value);

            fetch('/add-facilities', {
                method: 'post',
                body: data
            }).then(function (response){
                return response.text();
            }).then(function (data){
                
                 var delay = 3000;

                if(data === "success"){
                    toastr['success']("Added new Facilities");
                    document.getElementById("message").innerHTML = "Please wait, you are redirecting to the facility page.";
                    document.createElement("message").innerHTML = `<div class="spinner-grow" role="status">
</div>`;
                    setTimeout(function(){
                        window.location.replace('/facilities');
                    }, delay);
                    
                }
            })
        });

    </script>
@endsection