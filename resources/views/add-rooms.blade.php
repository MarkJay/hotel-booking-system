@extends('layouts.templates.template')
@section('title', 'Add Rooms')
@section('content')
<a href="/rooms" class="btn btn-info mt-5"><i class="fas fa-home"></i> Back</a>

<div id="container">
    <h1 class="text-center mt-5">Add New Rooms</h1>
    <form action="/add-rooms" method="post" enctype="multipart/form-data" class="mt-3">
        @csrf
        <div class="d-flex flex-row border bg-white p-5">
            <div class="mr-5">
        <div class="form-group">
        <label for="">Room Name:</label>
            <select name="category_id" id="category_name" class="form-control" >

            @foreach ($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
            
            @endforeach    
        </select>
        <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Quantity</label>
                <input type="text" name="room_quantity" class="form-control" id="room_quantity">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Bed Number</label>
            <select name="room_bed" class="form-control" id="room_bed">
                
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                
            </select>  
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Capacity</label>
            <select name="room_capacity" class="form-control" id="room_capacity">
                
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6 or More</option>
            </select>  
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Price</label>
                <input type="number" name="room_price" class="form-control" id="room_price">
            <span class="text-danger"></span>
        </div>
    </div>

        <div>
        <div class="form-group">
            <label for="">Image</label>
                <input type="file" name="imgPath" class="form-control" id="imgPath">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Description</label>
            <textarea name="room_description" id="room_description" cols="30" rows="10" class="form-control" placeholder="Room Description Here.."></textarea>
                {{-- <input type="text" name="" class="form-control" id=""> --}}
            <span class="text-danger"></span>
        </div>

        </div>
            
        </div>
        <button type="submit" class="btn btn-unique">Submit</button>
    </form>
        <p id="message"></p>
    </div>
        <script>
            
        </script>
@endsection

