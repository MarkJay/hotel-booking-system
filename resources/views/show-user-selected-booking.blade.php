@extends('layouts.templates.user-template')
@section('title', 'My Booking Details')
@section('content')
    

    <div class="container">
        <h1>My Booking details</h1>
        <div class="row">
            <div class="booking-details">
            
               <li class="table-header">
                <div class="col col-1">Booking Number</div>
                <div class="col col-2">Customer Name</div>
                <div class="col col-3">Email</div>
                <div class="col col-4">Booking Status</div>
                <div class="col col-5">Payment Status</div>
                <div class="col col-6"></div>

                   {{-- <tr>
                       <th>Booking Number</th>
                       <th>Name</th>
                       <th>Email</th>
                       <th>Status</th>
                       <th>Payment</th>
                       <th>Action</th>
                   </tr> --}}
                </li>
                   @foreach ($userBooking as $booking)
                   <li class="table-row">
                    <div class="col col-1"><span class="booking-number">{{ $booking->created_at->format('U') }}</span></div>
                    <div class="col col-2">{{ $booking->user->firstname }} {{ $booking->user->lastname }}</div>
                    <div class="col col-3">{{ $booking->user->email }}</div>
                    <div class="col col-4">{{ $booking->status->name }}</div>
                    <div class="col col-5">{{ $booking->payment->name }}</div>
                    <div class="col col-6"><a href="/user-booking-details/{{ $booking->id }}" class="moreDetailsBtn">Show Details</a>
                    </div>


                   </li>
                   @endforeach
                </div>
        </div>
    </div>



@endsection