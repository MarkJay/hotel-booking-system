@extends('layouts.templates.template')
@section('title', 'Booking Details')
@section('content')
<div class="container">
    <h1 class="text-center mt-3 mb-2">Booking Details</h1>
<h2 class="text-center">Booking Number: <span class="text-danger"> {{ $booking->created_at->format('U') }}</span></h2>

    <div class="container shadow p-3 mb-5 bg-white rounded">
        <div class="bookingdetails border border-secondary p-2 m-1">
            <h4 class="text-primary pl-1">User Details</h4>
            <p><strong>Firstname:</strong> {{ $booking->user->firstname }}</p>
            <p><strong>Lastname:</strong> {{ $booking->user->lastname }}</p>
            <p><strong>Email:</strong> {{ $booking->user->email }}</p>
            <p><strong>Booking Check-In Date:</strong> {{ $booking->check_in }} </p>
            <p><strong>Booking Check-Out Date:</strong> {{ $booking->check_out }} </p>
        </div>

        <div class="room-details border border-secondary p-2 m-1">

            <div id="roomDetails">
                <div>
            <h4 class="text-primary pl-1">Room Details</h4>
            <p><strong>Room Type:</strong> {{$booking->room->category->name}} </p>
            <p><strong>Room Price:</strong> <span class="text-warning font-weight-bold">{{ $booking->room->price }} </span> </p>
            <p><strong>Room Capacity:</strong> {{ $booking->room->capacity }}</p>
            <p><strong>Room Description:</strong> {{ $booking->room->description }}</p>
                   
            </div>
            <div>
            <p><strong>Room Image:</strong><br>
            <img src="{{ asset($booking->room->imgPath) }}" alt="" srcset="" width="400px" height="250px">
            </p>
            </div>
        </div>
        </div>
        <div class="admin-remarks border border-secondary p-2 m-1">
            <h4 class="text-primary pl-1">Admin Remarks</h4>
        <p><strong>Admin Remarks:</strong> {{ $booking->status->name }}</p>
        <p><strong>Message:</strong> {{ $booking->message }}</p>
        </div>
    </div>
    
        

</div>
@endsection