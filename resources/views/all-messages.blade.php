@extends('layouts.templates.template')
@section('title', 'All Messages')
@section('content')

<div id="container">
    <div class="card card-cascade narrower mt-5">
        <!--Card image-->
        <div
          class="view view-cascade gradient-card-header young-passion-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-center align-items-center">
          <h3 class="white-text mx-3 p-3 text-uppercase">All Messages</h3>  
        </div>
        <!--/Card image-->
        <div class="px-4">
            <div class="gradient-card-header">{{ $messages->links() }}</div>
          <div class="table-wrapper">
            <!--Table-->
            <table class="table table-hover mb-1">
      
              <!--Table head-->
              <thead class="text-uppercase text-center">
                <tr>
                    <th class="th-lg font-weight-bold">message id</th>
                  <th class="th-lg font-weight-bold">name</th>
                  <th class="th-lg font-weight-bold">email</th>
                  <th class="th-lg font-weight-bold">Date Received</th>
                </tr>
              </thead>
              <!--Table head-->
      
              <!--Table body-->
              <tbody class="text-center">
                 
                @foreach ($messages as $message)   
                <tr>
                <td>{{ $message->id }}</td>
                <td>{{ $message->name }}</td>
                <td>{{ $message->email }}</td>
                <td><span class="bg-secondary p-1 rounded text-white">{{ $message->created_at }}</span></td>
                <td>
                    <a href="/show-message/{{ $message->id }}" class="btn btn-blue-grey">Show Details</a> 

                </td>
                </tr>
                    
                
            @endforeach
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
          </div>
      
        </div>
      
      </div>
</div>

@endsection