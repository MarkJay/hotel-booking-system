@extends('layouts.templates.template')
@section('title', 'About Us page')
@section('content')

    @if (count($aboutpage) === 1)

        @foreach ($aboutpage as $about)
        <h2 class="text-center mb-5 mt-5">About Page Details</h2>
        <div class="col-lg-6 offset-lg-3">
        <strong>Description:</strong>
        <p>{{ $about->description }}</p>
        <strong>Image</strong>
        <p>
        <img src="{{ asset($about->imgPath) }}" alt="" width="500px" height="500px">
        </p>
         <form action="/delete-about-us-page" method="post">
            @csrf
            @method('DELETE')
         <input type="hidden" name="aboutpage_id" value="{{ $about->id }}">
         <button type="submit" class="btn btn-elegant" onclick="return confirm('Are you sure you want to delete this?')">Delete</button>
         <a href="/update-about-us-page/{{ $about->id }}" class="btn btn-light">update</a>
        </form>
        </div>
        @endforeach

        @else

        <a href="/create-about-us-page" class="btn btn-unique">Add About Us Page</a>

    @endif


@endsection