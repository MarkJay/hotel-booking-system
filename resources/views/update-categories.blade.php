@extends('layouts.templates.template')
@section('title', 'Add Room Categories')
@section('content')
<a href="/categories" class="btn btn-info">Back</a>
<div id="container">
    <h1 class="text-center mt-5">Update Room Categories</h1>
{{-- <form action="/update-categories/{{ $category->id }}" method="post">
        @csrf
        @method('PATCH') --}}
        <div class="form-group">
        <label for="">Name:</label>
        <input type="text" name="room_category" class="form-control" value="{{ $category->name }}" id="room_category">
        <span class="text-danger"></span>
        </div>
        <input type="hidden" name="category_id" value="" data="{{ $category->id }}">
        <button type="submit" class="btn btn-unique">Update</button>

        <p id="message"></p>
    {{-- </form> --}}
</div>
<script>
        const roomCategoryInput = document.getElementById('room_category');
        
        
        const submitBtn = roomCategoryInput.parentElement.nextElementSibling.nextElementSibling;
        console.log(submitBtn);
        submitBtn.disabled = true;
       
        function isEmpty(value, element){
            if(value === ''){
                element.nextElementSibling.textContent = "* This field is required";
            } else {
                element.nextElementSibling.textContent = "";
            }
        }

        function enabledButton(){
            if(roomCategoryInput.value === ''){
                submitBtn.disabled = true;
            }else {
                submitBtn.disabled = false;
            }
        }

       roomCategoryInput.addEventListener('blur', function(){
            roomCategoryValue = roomCategoryInput.value;
            isEmpty(roomCategoryValue, roomCategoryInput);
            enabledButton();
       });

       submitBtn.addEventListener('click', function(){
            const category_id = submitBtn.previousElementSibling.getAttribute('data');
            let data = new FormData;

            data.append('_token', "{{ csrf_token() }}")
            data.append('room_category', roomCategoryInput.value);          

            fetch('/update-categories/' + category_id, {
                method: 'post',
                body: data
            }).then(function (response){
                return response.text();
            }).then(function (data){
                var delay = 3000;
                document.getElementById('message').innerHTML = "Please wait, you will be redirecting to category page";
                
                if(data === "success"){
                    toastr['success']("Successfully Added New Category");
                    
                    setTimeout(function() {
                        window.location.replace('/categories');
                    }, delay);
                }
            });
       });

    </script>
@endsection