@extends('layouts.templates.template')
@section('title', 'Add Rooms')
@section('content')
<a href="/rooms"><i class="far fa-arrow-alt-circle-left"></i></a>

    <h1 class="text-center mb-3 mt-5">Update Room</h1>
    <div class="container">
        <div class="d-flex flex-row justify-content-center align-items-center">
    <form action="/update-rooms/{{ $rooms->id }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="form-group">
        <label for="">Room Name:</label>
            <select name="category_id" id="category_name" class="form-control">
            @foreach ($categories as $category)
            <option value="{{ $category->id }}" {{ $category->id === $rooms->category_id ? 'Selected' : '' }}>{{ $category->name }}</option>
            @endforeach    
        </select>
        <span class="text-danger"></span>
        </div>
        <div class="form-group">
        
            <label for="">Image</label>
                <input type="file" name="imgPath" class="form-control" id="imgPath">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            {{-- <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea> --}}
            <label for="">Description</label>
        <input type="text" name="room_description" class="form-control" id="room_description" value="{{ $rooms->description }}">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Quantity</label>
        <input type="text" name="room_quantity" class="form-control" id="room_quantity" value="{{ $rooms->quantity }}">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Bed Number</label>
        <input type="text" name="room_bed" class="form-control" id="room_bed" value="{{ $rooms->bed_room }}">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Capacity</label>
        <input type="text" name="room_capacity" class="form-control" id="room_capacity" value="{{ $rooms->capacity }}">
            <span class="text-danger"></span>
        </div>
        <div class="form-group">
            <label for="">Price</label>
        <input type="number" name="room_price" class="form-control" id="room_price" value="{{ $rooms->price }}">
            <span class="text-danger"></span>
        </div>
        
        <button type="submit" class="btn btn-unique">Submit</button>
    </form>
    <div class="image-container">
    <img src="{{ asset($rooms->imgPath) }}" alt="{{ asset($rooms->imgPath) }}" srcset="" height="500px" width="500px" class="ml-5"><br><span class="ml-5">{{ $rooms->imgPath }}</span>

        </div>
    </div>
</div>
        <p id="message"></p>

        <script>
            
        </script>
@endsection

