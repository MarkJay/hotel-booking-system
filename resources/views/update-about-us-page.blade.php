@extends('layouts.templates.template')
@section('title', 'About Page')
@section('content')

    <h1 class="text-center mt-5">About Page</h1>

<form action="/update-about-us-page/{{ $aboutpage->id }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" id="description" cols="30" rows="10" class="form-control" value="">{{ $aboutpage->description }}</textarea>
        </div>
            <div class="form-group">
                <label for="">image</label>
                <input type="file" name="imgPath" id="" class="form-control">
            <img src="{{ asset($aboutpage->imgPath) }}" alt="" srcset="" width="250px" height="250px" class="mt-2">
            </div>

            <button type="submit" class="btn btn-unique">Submit</button>
        </form>


@endsection