@extends('layouts.templates.template')
@section('title', 'Reports')
@section('content')

<div class="container">
    <h1 class="text-center mt-5">Reports</h1>

    <form action="/reports" method="POST" class="form-group col-lg-6 ">
       @csrf
        <label for="rooms">Rooms</label>
        <select name="room_category" id="room_category" class="form-control" >
            <option value="">All Rooms</option>
            @foreach ($categories as $category)
            
                <option value="{{ $category->id }}"> {{ $category->name }}</option>
            @endforeach 
        </select>
        <div class="form-group">
            <label for="from">From:</label>
            <input type="date" name="from" id="from" class="form-control" required="required">
        </div>
        <div class="form-group"> 
            <label for="to">To:</label> 
             <input type="date" name="to" id="to" class="form-control" required="required">
        </div>

        <button type="submit" class="btn btn-unique" id="generateBtn">Generate Report</button>
    </form>
    <div class="panel">
       
    </div>

    
</div>

<script>
    // const generateBtn = document.getElementById('generateBtn');
    // const fromInput = document.getElementById('from');
    // const toInput = document.getElementById('to');
    // const room = document.getElementById('room_category');	

    // let today = new Date();
    // let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    // const todaysDate = Date.parse(date);


    // generateBtn.addEventListener('click', function(){
    //     event.preventDefault();
    //     const fromDateValue = fromInput.value;
    //     const toDateValue = toInput.value;

    //     const from = Date.parse(fromDateValue);
    //     const to = Date.parse(toDateValue);

    //     if(fromInput.value === "" || toInput.value === ""){
    //             toastr['error']("Please input date");
    //     }
    //         else if(to < from){
    //             toastr['error']("Invalid date");
    //         } 
    //         else {

    //             let data = new FormData;

    //             data.append('_token', "{{ csrf_token() }}");
    //             data.append('from', fromInput.value);
    //             data.append('to', toInput.value);
    //             data.append('room_category', room.value);

    //             fetch('/reports', {
    //                 method: 'post',
    //                 body: data
    //             }).then(function(response){
                    
    //                 return response.text();
    //             }).then(function (data){
                    
    //                 if(data === 'success'){
    //                     window.location.replace('/booking-reports');
    //                 }else if (data === 'reports') {
    //                     window.location.replace('/booking-indiv-reports');
    //                 }
    //             });
   

    //         }

            
    // })

</script>
@endsection