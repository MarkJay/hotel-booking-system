<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin - Booking Management System</title>
     {{-- <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.3.1/materia/bootstrap.min.css"/>
     --}}
    {{-- Jquery --}}
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>
    {{-- Toaster JS --}}
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" defer></script>
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous" defer></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous" defer></script>

    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>
    <!-- Bootstrap tooltips -->
    {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js" defer></script> --}}
    <!-- Bootstrap core JavaScript -->
    {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" defer></script> --}}
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js" defer></script>

    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}"> 

    {{-- Toaster CSS --}}
     <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
   
     <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    
     
    
   
</head>

<body>

<div class="d-flex vh-100 flex-row">
        
    <div class="col-2 bg-dark adminDashboard">
            
            {{-- Admin NavBar --}}

        <div class="d-flex flex-column">
              <div class="avatar">
                    <img src="images/gon.jpg" alt="" srcset="" class="avatar-image">
              <h5 class="text-white text-center mt-2">Hello <span class="text-warning">{{ Auth::user()->firstname }}</span></h5>
              </div>
              <div class="nav">
                <div class="btn-group dropright">
                  <button type="button" class="btn btn-light">
                    Home
                  </button>
                  
                </div>
                <div class="btn-group dropright">
                    <button type="button" class="btn btn-light">
                      Room Category
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only"></span>
                    </button>
                    <div class="dropdown-menu">
                      <a href="/add-categories">Add New Room Category</a>
                      <a href="/categories">Manage Category</a>
                    </div>
                  </div>
                  <div class="btn-group dropright">
                    <button type="button" class="btn btn-light">
                      Room Facilities
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only"></span>
                    </button>
                    <div class="dropdown-menu">
                      <a href="/add-facilities">Add New Facilities</a>
                      <a href="/facilities">Manage Facilities</a>
                      <a href="/add-room-facilities">Add to Room</a>
                    </div>
                  </div>
                  <div class="btn-group dropright">
                    <button type="button" class="btn btn-light">
                      Roomes Rooms   
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only"><i class="fas fa-caret-right"></i></span>
                    </button>
                    <div class="dropdown-menu">
                      <a href="/add-rooms">Add New Room</a>
                      <a href="/rooms">Manage Room</a>
                    </div>
                  </div>
                  <div class="btn-group dropright">
                    <button type="button" class="btn btn-light">
                      Room Booking
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only"><i class="fas fa-caret-right"></i></span>
                    </button>
                    <div class="dropdown-menu">
                      <a href="/all-bookings">All Booking</a>
                      <a href="/new-bookings">New Booking</a>
                      <a href="/approved-bookings">Approved</a>
                      <a href="/cancelled-bookings">Cancelled</a>
                    </div>
                  </div>
                  <div class="btn-group dropright">
                    <button type="button" class="btn btn-light">
                      Register Users
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only"><i class="fas fa-caret-right"></i></span>
                    </button>
                    <div class="dropdown-menu">
                      <a href="/all-user-registered">All Registered User</a>
                      <a href="/admin-user">Add new admin-user</a>
                      <a href="/admin">Manage Admin</a>
                    </div>
                  </div>
                  <div class="btn-group dropright">
                    <button type="button" class="btn btn-light">
                      Reports &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only"><i class="fas fa-caret-right"></i></span>
                    </button>
                    <div class="dropdown-menu">
                      <a href="/reports">Generate Reports</a>
                      <a href="/search">Search Booking Details</a>
                    </div>
                  </div>
                  <div class="btn-group dropright">
                    <button type="button" class="btn btn-light">
                      Line Enquiry 
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only"><i class="fas fa-caret-right"></i></span>
                    </button>
                    <div class="dropdown-menu">
                      <a href="/messages">Manage Messages</a>
                    </div>
                  </div>
                  <div class="btn-group dropright">
                    <button type="button" class="btn btn-light">
                      Pages &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only"><i class="fas fa-caret-right"></i></span>
                    </button>
                    <div class="dropdown-menu">
                      <a href="/about-us-page">About Us Page</a>
                      <a href="/contact-us-page">Contact Us Page</a>
                    </div>
                  </div>
              </div>

              <form action="/logout" method="post">
                @csrf
               <button type="submit" class="btn btn-blue"> <i class="fas fa-power-off"></i><span> Logout</span></button>
            </form>
            </div>
            

        </div>
        <div class="col-10">
            {{-- <h1 class=""><span class="text-warning"> PACIFICA</span> BOOKING SYSTEM</h1> --}}
            
            <div class="col-lg-11 ml-5">
                
        @yield('content')
        
    </div>
 
  

</div>

<footer class="text-center fixed-bottom">
    <p class="">&copy; 2020 <span class="text-warning">PERLAS</span> HOTEL BOOKING SYSTEM</p>
</footer>

<script>



</script>
</body>
</html>
