<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- google fonts --}}
    <link href="https://fonts.googleapis.com/css?family=Lato:300,700|Prata" rel="stylesheet">
    
     {{-- Custom CSS --}}
     
    <link rel="stylesheet" href="{{ asset('css/user.css') }}">

    <link rel="stylesheet" href="https://cdn.tutorialjinni.com/izitoast/1.4.0/css/iziToast.css">
    <link rel="stylesheet" href="https://cdn.tutorialjinni.com/izitoast/1.4.0/css/iziToast.min.css">
    
    <script src="https://cdn.tutorialjinni.com/izitoast/1.4.0/js/iziToast.min.js" defer></script>
    <script src="https://cdn.tutorialjinni.com/izitoast/1.4.0/js/iziToast.js" defer></script>
    
    
    {{-- FontAwesome --}}
    <script src="https://kit.fontawesome.com/adee9377ac.js" crossorigin="anonymous" defer></script>
    

    <title>@yield('title')</title>
</head>
<body>
    <div class="navbar">
        <div class="contact">
            <div class="contact-information">
                <span>0987 6543 21</span> | <span>perlas@perlas.silanganan</span>
                
            </div>
            <div class="contact-social-media">
                {{-- <a href="/show-rooms" class="moreDetailsBtn">BOOK NOW</a> --}}
                <strong> FOLLOW US ON:  </strong>
                    <i class="fab fa-facebook-square"></i>
                    <i class="fab fa-instagram-square"></i>
                    <i class="fab fa-gitlab"></i>
                    <i class="fab fa-git"></i>
                    
            </div>
        </div>
        {{-- <hr> --}}
        <div class="logo-and-nav-container">
            <div class="logo-container">
                <h1>Perlas</h1>
                {{-- <img src="images/Slide3.png" alt="" srcset="" height="80px" width="100px"> --}}
            
                
            </div>
            <ul>
                
                <li><a href="/user-home">Home</a></li>
                <li><a href="/show-rooms">Rooms</a></li>
                <li><a href="/about-us">About Us</a></li>
                <li><a href="/contact-us">Contact Us</a></li>
                
                    @auth
                        @if (Auth::user()->role_id === 1)
                        <div class="dropdown">
                            <a href="">Hello <span id="textAmber">{{ Auth::user()->firstname }}</span> <i class="fas fa-caret-down"></i></a>
                                <div class="dropdown-content">
                                    <a href="/show-users-booking">My Booking</a>
                                    <a href="/user-profile">My Profile</a>
                                    <form action="/logout" method="post">
                                        @csrf
                                       <button type="submit" class="logoutBtn"> <span>Logout</span></button>
                                    </form>
                                 </div>  
                            </div> 
                            
                            
                        @endif
                        
                    @endauth

                    @if (!Auth::user())
                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Register</a></li> 
                    @endif
                    
            </ul>
        </div>
    </div>

    @yield('content')

    <div class="footer">
        <div class="contact-footer">
            <div class="logo-name">
                <h1>Perlas</h1>
                <p>We invite you to feel the luxury of the number one hotel choice of  asia. </p>
                    
                
                <div class="footer-logo-wrapper">
                <div class="cb">
                        <i class="fab fa-facebook-square"></i>
                    </div>
                    <div class="cb">
                        <i class="fab fa-instagram-square"></i>
                    </div>
                    <div class="cb">
                        <i class="fab fa-gitlab"></i>
                    </div>
                    <div class="cb">
                        <i class="fab fa-git"></i>
                    </div>
                </div>
            </div>
            <div class="footer-contact-information">
                <h3>CONTACT US</h3>
                <p><i class="fas fa-phone-alt"></i>0987 6543 21</p>    
                <p><i class="fas fa-map-marker-alt"></i> Manila, Philippines</p>    
                <p><i class="fas fa-envelope"></i>  perlas@perlas.silanganan</p>    
            </div>    
        </div>            
        <div class="footer-bottom"><p> &copy; 2020 This work is made with <i class="fas fa-heart-broken"></i> by Mark Jay. </p>
    </div>
</div>
</body>
</html>