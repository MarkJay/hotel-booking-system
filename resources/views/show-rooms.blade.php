@extends('layouts.templates.user-template')
@section('title', 'Rooms')
@section('content')

<div class="container">
        <h1>Our Rooms</h1>
    <div class="row">
        @foreach ($rooms as $room)
        <div class="card">
            <div class="card-header">
            <img src="{{ asset($room->imgPath) }}" alt="" srcset="" height="220px" width="340px">
            </div>
            <div class="card-body">
            <h2>{{$room->category->name }}</h2>
            <p><span class="price">&#8369;{{ $room->price }}</span>/per day</p>
            <p>Beds: {{ $room->bed_room }}</p>
            <p>Capacity: Max person {{ $room->capacity }}</p>
                <p id ="facilities">Facilities: @foreach ($room->facility as $facility)
                <span>{{ $facility->name }},</span>
                @endforeach</p>
                <a href="/show-selected-room/{{ $room->id }}" class="moreDetailsBtn">More Details</a> 
            </div>
         
        </div>
        @endforeach  
    </div>
    <div class="container">
        {{ $rooms->links() }}</div>
    </div>
   
@endsection