@extends('layouts.templates.template')
@section('title', 'Message')
@section('content')

<div class="container">
    <h1 class="text-center mt-5 mb-2">Message Details</h1>

<div class="user-booking-details">
    <div class="bookingdetails border border-secondary p-2 m-1">
            <h4 class="text-primary pl-1">User Details</h4>
                <p><strong>From: </strong> {{ $message->name }}</p>
                <p><strong>Email:</strong> {{ $message->email }}</p>
            </div>
            
</div>
<div class="bookingdetails border border-secondary p-2 m-1">
    <h4 class="text-primary pl-1">Message Details</h4>
    <p><strong>Message: </strong> {{ $message->message }}</p>
</div>
<div class="d-flex flex-column">
    <form action="/delete-message" method="post">
            @csrf
            @method('DELETE')
            <button type="button" class="btn btn-unique m-0" id="sendMessageBtn">
                Send a message
            </button>
                <input type="hidden" name="message_id" value="{{ $message->id }}">
                <button type="submit" class="btn btn-elegant m-0" onclick="return confirm('Are you sure you want to delete this message?')">
            Delete
        </button>
     </form>
    </div>
</div>

<script>

    
</script>

@endsection