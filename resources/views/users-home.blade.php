@extends('layouts.templates.user-template')
@section('title', 'Home')
@section('content')

<div class="w3-content w3-section">
    <img class="mySlides" src="https://res.cloudinary.com/dfwctifj7/image/upload/v1598344960/t4rnvm11tizdusvza6e0.jpg" style="width:100%">
    <img class="mySlides" src="https://res.cloudinary.com/dfwctifj7/image/upload/v1598345024/f0zzxtwnctdobuxyx9xh.jpg" style="width:100%">
    <img class="mySlides" src="https://res.cloudinary.com/dfwctifj7/image/upload/v1597062425/ygjuuh4smujoign9ae6y.jpg" style="width:100%">
    
    <div class="form-container">
        {{-- <form action="/check-availability" method="post">
            @csrf --}}
            <h3>Your Reservation</h3>
            <div class="form-group check_in">
                <label for="check_in">Check In</label>
                <input type="date" name="check_in" id="check_in" required="Required" class="form-control" placeholder="Select suitable date">
            </div>
            <div class="form-group check_out">
                <label for="check_out">Check Out</label>
                <input type="date" name="check_out" id="check_out" required="Required" class="form-control" placeholder="Select suitable date">
            </div>
            <div class="form-group">
                <label for="Rooms">Rooms</label>
                <select name="room_id" id="room_id" class="form-control">
                    @foreach ($rooms_categories as $room_category)
                <option value="{{ $room_category->id }}">{{ $room_category->name }}</option>
                    @endforeach
                </select>
                <div class="inputs">

                </div>
            </div>
           
             <div class="form-group guest">
                <label for="guest">Guest</label><br>
                <label for="Adult">Adult</label>
                <select name="adult" id="adult" class="form-control">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <label for="Children">Children</label>
                <select name="children" id="children" class="form-control">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
            
            <button type="submit" class="btn bookingBtn" id="bookingBtn">Check Availability</button>
        {{-- </form> --}}
    
    </div>
  </div>
    
<div class="about">
    <h1>About Us</h1>
    <div class="about-us">
    <div class="about-us-content">
        <h3>When you come to a hotel room, you want it to be grand, <br>functional and beautiful. But you don't want things that are not <br>useful. Sometimes you go to hotels and there are all these frames <br>and pictures of people you don't know, and you end up hiding <br>everything in the drawer, and then housekeeping<br> come and put it out again. </h3>
        <sub>Diane Von Furstenberg</sub>
    </div>
    <div class="about-us-image">
        <img src="https://res.cloudinary.com/dfwctifj7/image/upload/v1598345169/sv9vcg7ftd85msgzrcaj.jpg" alt="" srcset="" width="500px" height="350px">
    </div>

</div>
<hr>
<div class="room-special">
    @foreach ($rooms as $room)
    <div class="image-1">
    <img src="{{ asset($room->imgPath) }}" alt="" srcset="">
    <div class="image__overlay">
        <h2>{{$room->category->name }}</h2>
        <p><span class="price">&#8369;{{ $room->price }}</span>/per day</p>
            <p>Beds: {{ $room->bed_room }}</p>
            <p>Capacity: Max person {{ $room->capacity }}</p>
                <p id ="facilities">Facilities: @foreach ($room->facility as $facility)
                <span>{{ $facility->name }},</span>
                @endforeach
            </p>
            <a href="/show-selected-room/{{ $room->id }}" class="moreDetailsBtn"> More Details</a>
    </div>
    </div>
    
    @endforeach
    

</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){

        $(document).on('change', '#room_id', function(){
            console.log('testing');

            var id=$(this).val();

            var div = $(this).parent();
            
            
            var inputQuantity=" ";
            var inputCapacity=" ";
            var inputCategory=" ";
            var inputRoom=" ";

            $.ajax({
                type: 'get',
                url: '{!! URL::to('findCategoryName') !!}',
                data: {'id': id},
                success:function(data){

                    console.log(data);

                    room_quantity = data[0].quantity;
                    room_capacity = data[0].capacity;
                    room_category = data[0].category_id;
                    roomId = data[0].id;
                    
                    console.log(room_category);
                    inputQuantity +=`<input type="hidden" id="room_quantity" name="room_quantity" value="${room_quantity}">`;
                    inputCapacity +=`<input type="hidden" id="room_capacity" name="room_capacity" value="${room_capacity}">`;
                    inputCategory +=`<input type="hidden" id="room_category" name="room_category" value="${room_category}">`;
                    inputRoom +=`<input type="hidden" id="roomId" name="roomId" value="${roomId}">`;

                    div.find('.inputs').html("");

                    // console.log(div.find('.inputs').html(""));
                    div.find('.inputs').append(inputCapacity);
                    div.find('.inputs').append(inputQuantity);
                    div.find('.inputs').append(inputCategory);
                    div.find('.inputs').append(inputRoom);

                    // console.log(div.find('.inputs').append(input));
                },
                error:function(){

                }
            });
        })

    })

        var myIndex = 0;
        carousel();

        function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";  
        }
        myIndex++;
        if (myIndex > x.length) {myIndex = 1}    
        x[myIndex-1].style.display = "block";  
        setTimeout(carousel, 5000); 
}
    // let checkin = new Date().toISOString().split('T')[0];
    // document.getElementsByName("check_in")[0].setAttribute('min', checkin);
    

    const checkInInput = document.getElementById('check_in');
    const checkOutInput = document.getElementById('check_out');
    const bookingBtn = document.getElementById('bookingBtn');
    const room_id = document.getElementById('room_id');
    let room_quantity = document.getElementById('room_quantity');
    
    const adult = document.getElementById('adult');
    const children = document.getElementById('children');

    


    let today = new Date();
    let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    const todaysDate = Date.parse(date);

    //validate of check in date is valid;

    bookingBtn.addEventListener('click', function (){
    
    const checkInValue = checkInInput.value;
    const checkOutValue = checkOutInput.value;
    
    const checkIn = Date.parse(checkInValue);
    const checkOut = Date.parse(checkOutValue);
    
    
    
    if(checkInInput.value === "" || checkOutInput.value === ""){
        iziToast.warning({
                        title: 'For Reservation:',
                        message: 'Please input a date',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });
    }
    else if(checkIn < todaysDate){
        iziToast.warning({
                        title: '',
                        message: 'Wag mo na balikan ang kahapon',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });
    }
    else if (checkOut < checkIn){
            iziToast.error({
                        title: 'Error',
                        message: 'Invalid date, Please input a correct date',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });
        } 

        else {

            let result = adult.value + children.value;
            let roomId = document.getElementById('roomId');
            let room_capacity = document.getElementById('room_capacity');
            let roomValue = roomId.value;
            let roomCapacity = room_capacity.value;

            let data = new FormData;

            data.append('_token', "{{ csrf_token() }}");
            data.append('check_in', checkInInput.value);
            data.append('check_out', checkOutInput.value);
            data.append('room_capacity', room_capacity.value);
            data.append('room_quantity', room_quantity.value);
            data.append('room_id', room_id.value);
            

            fetch('/check-availability', {
                method: 'post',
                body: data
            }).then(function (response){
                return response.text();
            }).then(function (data){
                if(data === "not available"){
                    iziToast.error({
                        title: 'Room',
                        message: 'Room is not available, Check for another date',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });
                } 
                else  {

                    if(result >= roomCapacity){
                        iziToast.error({
                        title: 'Room',
                        message: `This room has Maximum Capacity of ${roomCapacity}`,
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });
                    }

                    else if(data === "available"){

                        var delay = 3000;

                        iziToast.success({
                        title: 'Room',
                        message: 'Room is available, You will redirecting to room page',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });

                    setTimeout(function() {
                        window.location.replace('/show-selected-room/' + roomValue );
                    }, delay);

                    }
                    
                }
            
                checkInInput.value = "";
                checkOutInput.value = "";

            });
        }
        
    }) 


</script>
@endsection