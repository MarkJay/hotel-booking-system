@extends('layouts.templates.template')
@section('title', 'Add Room Categories')
@section('content')

<div id="container">
    <h1 class="text-center mt-5">Update Facilities</h1>
{{-- <form action="/update-facilities/{{ $facility->id }}" method="post">
        @csrf --}}
        
        <div class="form-group">
        <label for="">Name:</label>
        <input type="hidden" name="facility_id" data="{{ $facility->id }}">
        <input type="text" name="facility_name" class="form-control" value="{{ $facility->name }}" id="facility">
        <span class="text-danger"></span>
        </div>
        
        <button type="submit" class="btn btn-unique">Update</button>

        <p class="" id="message"></p>
    {{-- </form> --}}
</div>
    <script>
        const facilityInput = document.getElementById('facility');
        const submitBtn = facilityInput.parentElement.nextElementSibling;
        
        submitBtn.disabled = true;

        function isEmpty(value, element){
            facilityInputValue = facilityInput.value;
            if(facilityInputValue === ""){
                element.nextElementSibling.textContent = "Please input facilities name";
            }else {
                element.nextElementSibling.textContent = "";
            }

        }
        function enableButton(){
            if(facilityInput.value === ""){
                submitBtn.disabled = true;
            }else {
                submitBtn.disabled = false;
            }
        }

        facilityInput.addEventListener('blur', function(){
            facilityInputValue = facilityInput.value;

            isEmpty(facilityInputValue, facilityInput);
            enableButton();
        });

        submitBtn.addEventListener('click', function(){
            const facilityValue = facilityInput.value;
            const facility_id = facilityInput.previousElementSibling.getAttribute('data');

            let data = new FormData;

            data.append('_token','{{csrf_token() }}')
            data.append('facility_name', facilityInput.value);

            fetch('/update-facilities/' + facility_id, {
                method: 'post',
                body: data
            }).then(function (response){
                return response.text();
            }).then(function (data){

                var delay = 3000;
                var timeout = 1000;
                

                if(data === "success"){
                    toastr['success']("Updated facility");
                    setTimeout(function(){
                        window.location.replace('/facilities');
                    }, delay);

                    setTimeout(() => {
                        document.getElementById("message").innerHTML = "Please wait, you are redirecting to the facility page.";
                }, timeout);
                    
                }
            })
        });

    </script>
    
@endsection