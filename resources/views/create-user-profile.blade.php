@extends('layouts.templates.user-template')
@section('title', 'My Profile')
@section('content')

<div class="container">
    <h1>My Profile</h1>

    <form action="/create-profile" method="post" class="form-control" enctype="multipart/form-data">
    @csrf
    <div class="profile-form">
    <label for="">Address</label>
    <input type="text" name="address" id="address" placeholder="Your address" class="form-control">
    <label for="">Number</label>
    <input type="number" class="form-control" name="contact_number" id="contact_number" placeholder="Your Contact Number">
    <label for="">Profile Image</label>
    <input type="file" name="imgPath" id="imgPath">
    <button type="submit" class="moreDetailsBtn">Create My Profile</button>
    </div>
    
    
    </form>


   


</div>
@endsection