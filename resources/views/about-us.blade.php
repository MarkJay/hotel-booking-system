@extends('layouts.templates.user-template')
@section('title', 'About Us')
@section('content')

    <div class="container">
        <h1>About Us</h1>

        <div class="about user">
            <div class="about-us">
                <div class="about-us-content user">
                        @foreach ($aboutpage as $about)
                        <h3> {{ $about->description  }} </h3>
                        <sub>Diane Von Furstenberg</sub>
                </div>
                <div class="about-us-image">
                    <img src="{{ asset($about->imgPath) }}" alt="" srcset="" width="500px" height="350px">
                </div>
                    @endforeach
                
            </div>
        </div>
        
        <h1>Our Services</h1>
        <div class="facility-wrapper">
            
            <div class="facility">
                
            @foreach ($facilities as $facility)
            
                <p><i class="fab fa-500px"></i> {{ $facility->name }} </p>
            @endforeach
            </div>
        </div>
    <div class="timezone-wrapper">
        <h1>World Timezone</h1>
        <div class="time">
            <div id="antarctica-wrapper">
                <h2 id="antarctica"></h2>
                    <div id="clock"></div>
                </iframe>
            </div>
            <div class="costa-rica-wrapper">
                <h2 id="costa-rica"></h2>
                <div id="costa-clock"></div>
            </div>
            <div class="philippines-wrapper">
                <h2 id="philippines"></h2>
                <div id="philippines-clock"></div>
            </div>
        </div>
    </div>
    </div>

<script>

fetch("http://worldtimeapi.org/api/timezone/Indian/Maldives", {
	"method": "GET",
})
.then(function(response) {
	return response.json();
}).then(function(data){
    let time = data.unixtime;
    let newtime = new Date(time*1000).toString();
    
    // const rtClock = new Date(newtime);

    // let hours = rtClock.getHours();
    // let minutes = rtClock.getMinutes();
    // let seconds = rtClock.getSeconds();

    // let amPm = ( hours < 12 ) ? "AM" : "PM";
    
    // hours = (hours < 12) ? hours - 12 : hours;
    
    // hours = ("0" + hours).slice(-2);
    // minutes = ("0" + minutes).slice(-2);
    // seconds = ("0" + seconds).slice(-2);
    
    document.getElementById('philippines').innerHTML = data.timezone;
    document.getElementById('philippines-clock').innerHTML = newtime;
    // document.getElementById('philippines-clock').innerHTML = `${hours} :  ${minutes} : ${seconds} ${amPm}`;
    

});

fetch("http://worldtimeapi.org/api/timezone/Asia/Manila", {
	"method": "GET",
})
.then(function(response) {
	return response.json();
}).then(function(data){
    let time = data.unixtime;
    let newtime = new Date(time*1000).toUTCString();
    
    // const rtClock = new Date(newtime);

    // let hours = rtClock.getHours();
    // let minutes = rtClock.getMinutes();
    // let seconds = rtClock.getSeconds();

    // let amPm = ( hours < 12 ) ? "AM" : "PM";
    
    // hours = (hours < 12) ? hours - 12 : hours;
    
    // hours = ("0" + hours).slice(-2);
    // minutes = ("0" + minutes).slice(-2);
    // seconds = ("0" + seconds).slice(-2);
    
    document.getElementById('costa-rica').innerHTML = data.timezone;
    document.getElementById('costa-clock').innerHTML = newtime;
    // document.getElementById('costa-clock').innerHTML = `${hours} :  ${minutes} : ${seconds} ${amPm}`;
    

});

fetch("http://worldtimeapi.org/api/timezone/Europe/Amsterdam", {
	"method": "GET",
})
.then(function(response) {
	return response.json();
}).then(function(data){
    
    
    let time = data.datetime;
    let newtime = new Date().toString();
    
    // const rtClock = new Date(newtime);

    // let hours = rtClock.getHours();
    // let minutes = rtClock.getMinutes();
    // let seconds = rtClock.getSeconds();

    // let amPm = ( hours < 12 ) ? "AM" : "PM";
    
    // hours = (hours < 12) ? hours - 12 : hours;
    
    // hours = ("0" + hours).slice(-2);
    // minutes = ("0" + minutes).slice(-2);
    // seconds = ("0" + seconds).slice(-2);
    
    document.getElementById('antarctica').innerHTML = data.timezone;
    document.getElementById('clock').innerHTML = newtime;
    // document.getElementById('clock').innerHTML = `${hours} :  ${minutes} : ${seconds} ${amPm}`;
    




});


</script>
@endsection