<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table class="table table-striped border">
        <thead>
            <tr>
                <th>Booking Id</th>
                <th>Booking Date</th>
                <th>Booking User</th>
                <th>Booking Room</th>
                <th>Total</th>
                <th>Payment</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($bookings as $booking)
            <tr>
                
            <td>{{ $booking->created_at }}</td>
            <td>{{ $booking->created_at }}</td>
            <td>{{ $booking->firstname }} {{ $booking->lastname }}</td>
            <td>{{ $booking->name }}</td>
            <td>{{ $booking->total }}</td>
            <td>{{ $booking->payment_id }}</td>
            <td>{{ $booking->status_id }}</td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</body>
</html>