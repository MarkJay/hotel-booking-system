@extends('layouts.templates.template')
@section('title', 'Add Facility to Room')
@section('content')



<a href="/facilities" class="btn btn-info mt-5"><i class="fas fa-home"></i> Back</a>
    <h1 class="text-center mb-5">Add new Facility Room</h1>
<div class="d-flex justify-content-center align-items-center flex-column form-group">
    <form>     
        <div class="form-group">
        <label for="Rooms">Select Room</label>
            <select name="room_id" id="room_id" class="form-control"> 
                    @foreach (App\Room::all() as $room)
                        <option value="{{ $room->id }}">{{ $room->category->name }}</option>
                    @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="">Select Facility</label>
            <select name="facility_id" id="facility_id" class="form-control">
                    @foreach ($facilities as $facility)
                        <option value="{{ $facility->id }}">{{ $facility->name }}</option>
                    @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-unique" id="addfacilityBtn">Submit</button>
    </form>
</div>

<div class="d-flex justify-content-center align-items-center flex-row mt-1" id="shareDetails">
    
    @foreach (App\Room::all() as $room)
    <ul class="list-group ml-1" >
            <li class="list-group-item bg-dark text-white">{{ $room->category->name }}</li>
            @foreach ($room->facility as $facility)
            <li class="list-group-item d-flex justify-content-between align-items-center text-center">
                   {{ $facility->name }}
                   <form action="/remove-facility" method="post">
                    @csrf
                        <input type="hidden" name="room_id" value="{{ $room->id }}">
                        <input type="hidden" name="facility_id" value="{{ $facility->id }}">
                    <button type="submit" class="btn btn-elegant" onclick="return confirm('Are you sure you want to delete this facility?')">Delete</button>
                </form>  
            </li>
            @endforeach
    </ul>
    @endforeach

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>
//     $(document).ready(function(){

// $(document).on('change', '#room_id', function(){
//     console.log('testing');

//     var id=$(this).val();


//     $.ajax({
//         type: 'get',
//         url: '{!! URL::to('findRoomName') !!}',
//         data: {'id': id},
//         success:function(data){

//             console.log(data);

//             // console.log(div.find('.inputs').append(input));
//         },
//         error:function(){

//         }
//     });
// })

// });
    

    const addFacilityBtn = document.getElementById('addfacilityBtn');
    const room = document.getElementById('room_id');
    const facility = document.getElementById('facility_id');

    addFacilityBtn.addEventListener('click', function(){
        
        
        let data = new FormData;

        data.append('_token', "{{ csrf_token() }}");
        data.append('room_id', room.value);
        data.append('facility_id', facility.value);


        fetch('/add-room-facilities', {
            method: 'post',
            body: data
        }).then(function (response){
            return response.text();
            
        }).then(function (data){
            var delay = 3000;

            console.log(data);
            if(data === "success"){
                toastr['success']('Successfully Added Room Facility');
    
            } else if (data === "duplicate"){
                toastr['error']('Facility is already exist to this room');
            }
            
            // let name = data;

            // let dataFromFetch = JSON.parse(name);

            // for(let i = 0; i < dataFromFetch.length; i++){
                
            //     let facilityName = dataFromFetch[i].name;

                
            //     var div = document.createElement('ul');
 
            //     div.textContent = facilityName;
            //     div.setAttribute('class', 'list-group-item d-flex justify-content-between align-items-center text-center');
            //     document.getElementById('shareDetails').appendChild(div);
            // }
            
        });
        
        

        event.preventDefault();

        

    })

</script>
@endsection