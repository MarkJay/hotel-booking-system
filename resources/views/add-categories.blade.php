@extends('layouts.templates.template')
@section('title', 'Add Room Categories')
@section('content')
<a href="/categories" class="btn btn-info mt-5"><i class="fas fa-home"></i> Back</a>
<div id="container">
    <h1 class="text-center mt-5">Add Room Categories</h1>
    {{-- <form action="/add-categories" method="post">
        @csrf --}}
        <div class="form-group">
        <label for="">Name:</label>
        <input type="text" name="room_category" class="form-control" id="room_category">
        <span class="text-danger"></span>
        </div>
        
        <button type="submit" class="btn btn-unique">Submit</button>

        <p id="message"></p>
    {{-- </form> --}}
</div>
    <script>
        const roomCategoryInput = document.getElementById('room_category');

        const submitBtn = roomCategoryInput.parentElement.nextElementSibling;

        submitBtn.disabled = true;
       
        function isEmpty(value, element){
            if(value === ''){
                element.nextElementSibling.textContent = "* This field is required";
            } else {
                element.nextElementSibling.textContent = "";
            }
        }

        function enabledButton(){
            if(roomCategoryInput.value === ''){
                submitBtn.disabled = true;
            }else {
                submitBtn.disabled = false;
            }
        }

       roomCategoryInput.addEventListener('blur', function(){
            roomCategoryValue = roomCategoryInput.value;
            isEmpty(roomCategoryValue, roomCategoryInput);
            enabledButton();
       });


       submitBtn.addEventListener('click', function(){
            
            let data = new FormData;

            data.append('_token', "{{ csrf_token() }}")
            data.append('room_category', roomCategoryInput.value);          

            fetch('/add-categories', {
                method: 'post',
                body: data
            }).then(function (response){
                return response.text();
            }).then(function (data){
                
                var delay = 3000;

                if(data === "duplicate"){
                    toastr['error']("Duplicate Entry");
                }
                if(data === "success"){
                    toastr['success']("Added New Category");
                    document.getElementById('message').innerHTML = "Please wait, you will be redirecting to category page";
                    setTimeout(function() {
                        window.location.replace('/categories');
                    }, delay);
                }
            });
       });

    </script>
@endsection