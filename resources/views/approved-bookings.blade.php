@extends('layouts.templates.template')
@section('title', 'All bookings')
@section('content')

    <div id="container">
        
    
    <div class="card card-cascade narrower mt-5">
        <!--Card image-->
        <div
          class="view view-cascade gradient-card-header young-passion-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-center align-items-center">
          <h3 class="white-text mx-3 p-3 text-uppercase">Approved bookings</h3>  
        </div>
        <!--/Card image-->
        <div class="px-4">
            <div class="gradient-card-header">{{ $bookings->links() }}</div>
          <div class="table-wrapper">
            <!--Table-->
            <table class="table table-hover mb-1">
      
              <!--Table head-->
              <thead class="text-uppercase text-center">
                <tr>
                  <th class="th-lg font-weight-bold">Booking ID</th>
                  <th class="th-lg font-weight-bold">Booking Room</th>
                  <th class="th-lg font-weight-bold ">Booking Check In</th>
                  <th class="th-lg font-weight-bold">Booking Check Out</th>
                  <th class="th-lg font-weight-bold">Date Approved</th>
                  <th class="th-lg font-weight-bold">Booking Status</th>
                  <th class="th-lg font-weight-bold"></th>
                </tr>
              </thead>
              <!--Table head-->
      
              <!--Table body-->
              <tbody>
                @foreach ($bookings as $booking)
                <tr>
                  <td class="text-danger">{{ $booking->created_at->format('U') }}</td>
                  <td class="roomDescription">{{ $booking->room->category->name }}</td>
                    <td><span class="p-2 bg-info text-white rounded"> {{ $booking->check_in }}</span></td>
                    <td><span class="p-2 bg-info text-white rounded"> {{ $booking->check_out }}</span></td>
                    <td>{{ $booking->updated_at }}</td>
                    <td class="text-uppercase">{{ $booking->status->name }}</td>
                    <td>
                        <a href="/show-booking-details/{{ $booking->id }}" class="btn btn-blue-grey">Show Details</a>
                    </td>
                    
                </tr>
            @endforeach
              </tbody>
              <!--Table body-->
            </table>
            <!--Table-->
          </div>
      
        </div>
      
      </div>
</div>
@endsection