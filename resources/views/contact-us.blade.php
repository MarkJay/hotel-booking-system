@extends('layouts.templates.user-template')
@section('title', 'Contact Us')
@section('content')

    <div class="container">
        <h1>Contact Us</h1>

        <div class="contact-wrapper">
            <div class="contact-message-container">
                <form  class="form-message">
                    <div class="user">
                        <i class="fas fa-user"></i>
                        <input type="text" name="name" id="name" placeholder="Your name">
                    </div>
                    <div class="email-message">
                        <i class="fas fa-envelope"></i>
                        <input type="text" name="email" id="email" placeholder="Your email">
                    </div>
                    <div class="user-message">
                        <i class="fas fa-pencil-alt"></i>
                        <textarea name="message" id="message" cols="30" rows="10" placeholder="Send message"></textarea>
                    </div>
                    <button id="messageBtn" class="moreDetailsBtn">Submit</button>
                </form>
            </div>
            <div class="contact-us-information">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61779.80240932983!2d120.98788852321303!3d14.585529707745597!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c90747a11ec7%3A0xac8cb4f985607d57!2sPerlas%20ng%20Silanganan!5e0!3m2!1sen!2sph!4v1598172653484!5m2!1sen!2sph" 
                width="620" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                <div class="social-contact-information">
                    <div class="sb location">
                        <div class="icons">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <span> 1234 Manila, Philippines</span>
                    </div>
                    <div class="sb contact-number">
                        <div class="icons">
                            <i class="fas fa-phone-alt"></i>
                    </div>
                         <span> 0987 - 654 - 321</span>
                    </div>
                    <div class="sb email">
                        <div class="icons">
                            <i class="fas fa-envelope"></i>
                        </div>
                         <span> perlas@perlas.silanganan</span>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        
            const name = document.getElementById('name');
            const email = document.getElementById('email');
            const message = document.getElementById('message');
            const messageBtn = document.getElementById('messageBtn');
        

        messageBtn.addEventListener('click', function(){
            
            
            

            let data = new FormData;

            data.append('_token', "{{ csrf_token() }}");
            data.append('name', name.value);
            data.append('email', email.value);
            data.append('message', message.value);
            
            fetch('/send-message', {
                method: 'post',
                body: data
            }).then(function (response){
                console.log(response)
                return response.text();
            }).then(function (data){
                var delay = 3000;

                console.log(data)

                if(data === "success"){
                    iziToast.success({
                        title: 'Message',
                        message: 'Successfully sent the message to admin',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });

                    setTimeout(function() {
                        window.location.replace('/contact-us');
                    }, delay);
                    
                }

            });

            event.preventDefault();
            name.innerHTML = "";
            email.innerHTML = "";
            message.innerHTML = "";
            
        })

    </script>

@endsection