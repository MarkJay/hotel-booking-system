@extends('layouts.templates.template')
@section('title', 'Booking Details')
@section('content')
<div class="container">
    <h1 class="text-center mt-3 mb-2">Booking Details</h1>
<h2 class="text-center">Booking Number: <span class="text-danger"> {{ $booking->created_at->format('U') }}</span></h2>

<div class="container shadow p-3 mb-5 bg-white rounded">
  <div class="bookingdetails border border-secondary p-2 m-1">
      <h4 class="text-primary pl-1">User Details</h4>
      <p><strong>Firstname:</strong> {{ $booking->user->firstname }}</p>
      <p><strong>Lastname:</strong> {{ $booking->user->lastname }}</p>
      <p><strong>Email:</strong> {{ $booking->user->email }}</p>
      <p> <strong>Booking Check-In Date:</strong> {{ $booking->check_in }} </p>
      <p> <strong>Booking Check-Out Date:</strong> {{ $booking->check_out }} </p>
      <p> <strong>Payment: </strong> {{ $booking->payment->name }} </p>
      <p> <strong>Total: </strong> <span class="text-warning font-weight-bold">&#8369; {{ $booking->total }} </span></p>
  </div>

  <div class="room-details border border-secondary p-2 m-1">

      <div id="roomDetails">
          <div>
      <h4 class="text-primary pl-1">Room Details</h4>
      <p><strong>Room Type:</strong> {{$booking->room->category->name}} </p>
      <p><strong>Room Price:</strong> <span class="text-warning font-weight-bold">{{ $booking->room->price }} </span> </p>
      <p><strong>Room Capacity:</strong> {{ $booking->room->capacity }}</p>
      <p><strong>Room Description:</strong> {{ $booking->room->description }}</p>
             
      </div>
      <div>
      <p><strong>Room Image:</strong><br>
      <img src="{{ asset($booking->room->imgPath) }}" alt="" srcset="" width="400px" height="250px">
      </p>
      </div>
  </div>
  </div>
  <div class="admin-remarks border border-secondary p-2 m-1">
      <h4 class="text-primary pl-1">Admin Remarks</h4>
  <p><strong>Admin Remarks:</strong> {{ $booking->status->name }}</p>
  <p><strong>Message:</strong> {{ $booking->message }}</p>
  </div>
</div>
    
<!-- Button trigger modal -->

@if ($booking->status_id === 1)
<button type="button" class="btn btn-unique m-0" data-toggle="modal" data-target="#exampleModal" >
  Take Action
</button>    
@endif

  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Booking Details</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            {{-- <form action="/update-booking" method="POST">
            @csrf
            @method('PATCH') --}}
            <input type="hidden" name="booking_id" id="booking_id" data="{{ $booking->id }}" require="Required">
            <label for="message">Message to Client</label><br>
          <span class="text-danger"></span>
          <textarea name="admin_remarks" id="admin_remarks" class="form-control mb-3 p-1" placeholder="Message here.."></textarea>
         
          <select name="status_id" id="status_id" class="form-control">
              @foreach ($statuses as $status)
          <option value="{{ $status->id }}"{{ $status->id === $booking->status_id ? 'Selected' : '' }}>{{ $status->name }}</option>
              @endforeach
          </select>
          <div class="modal-footer">
            <button type="submit" class="btn btn-dark" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-unique" id="submitBtn">Save changes</button>
          </div>
        {{-- </form> --}}
        </div> 
      </div>
    </div>
  </div>
</div>

<script>
  const submitBtn = document.getElementById('submitBtn');
  const messageInput = document.getElementById('admin_remarks');
  const statusInput = document.getElementById('status_id');
  const bookingInput = document.getElementById('booking_id');



  submitBtn.disabled = true;

  function enabledButton(){
    if(messageInput.value === ''){
      submitBtn.disabled = true;
    } else {
      submitBtn.disabled = false;
    }
  }
  messageInput.addEventListener('blur', function (){
    
    if(messageInput.value === ""){
      messageInput.previousElementSibling.textContent = `Please leave a remarks to user`;
    }
    else {
      messageInput.previousElementSibling.textContent = "";
    }
    enabledButton();
  })

  submitBtn.addEventListener('click', function(){
  booking_id = bookingInput.getAttribute('data');

  console.log(booking_id);
      let data = new FormData;

      data.append('_token', "{{ csrf_token() }}")
      data.append('admin_remarks', messageInput.value);
      data.append('status_id', statusInput.value);
      data.append('booking_id', booking_id);

      fetch('/update-booking', {
        method: 'post',
        body: data
      }).then(function (response){
        return response.text();
      }).then(function (data){
          if(data === "success"){
            window.location.replace('/show-booking-details/' + booking_id)
          }
      });

  });


</script>
@endsection