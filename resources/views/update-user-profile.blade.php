@extends('layouts.templates.user-template')
@section('title', 'My Profile')
@section('content')



<div class="container">
    <h1>My Profile</h1>

    
    <form action="/update-profile/{{ $profile->id }}" method="POST" class="form-control" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="profile-form">
        <label for="">FirstName</label>
        <input type="text" name="firstname" id="" value="{{ $profile->user->firstname }}">
        <label for="">LastName</label>
        <input type="text" name="lastname" id="" value="{{ $profile->user->lastname }}">
        <label for="">Email</label>
        <input type="text" name="email" id="" value="{{ $profile->user->email }}">

        <label for="">Address</label>
        <input type="text" name="address" id="address" placeholder="Your address" class="form-control" value="{{ $profile->address }}">
        <label for="">Number</label>
        <input type="number" class="form-control" name="contact_number" id="contact_number" placeholder="Your Contact Number" value="{{ $profile->number }}">
        <label for="">Profile Image</label>
        <input type="file" name="imgPath" id="imgPath">
        <img src="{{ asset($profile->imgPath) }}" alt="" width="100px" height="100px">
    
        <button type="submit" class="moreDetailsBtn">Create My Profile</button>
    </div>
        </form>
   

    
</div>
@endsection