@extends('layouts.templates.user-template')
@section('title', 'Rooms')
@section('content')
    

    <div class="container">
        <h1>Room Details</h1>
        <div class="selected-room-container">
            <div class="image-container">
                <img src="{{ asset($rooms->imgPath) }}" alt="" srcset="" height="450px" width="700px">
                
                <div class="title">
                    <h2>{{$rooms->category->name }}</h2>
                    <div class="stars">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </div>
                
                </div>
                
                <p><span class="price">&#8369;{{ $rooms->price }}</span>/per day</p>
                <p>Beds: {{ $rooms->bed_room }}</p>
                <p>Capacity: Max person {{ $rooms->capacity }}</p>
                <p>Facilities: @foreach($rooms->facility as $facility)
                <span>{{ $facility->name }},</span>
                    @endforeach
            </p>
                <p>Description</p>
                <p>{{ $rooms->description }}</p>
            </div>
            <div class="booking-form">
                <h3>Your Reservation</h3>
                    <div class="form-group check_in">
                        <label for="check_in">Check In</label>
                        <input type="date" name="check_in" id="check_in" required="Required" class="form-control" placeholder="Select suitable date">
                    </div>
                    <div class="form-group check_out">
                        <label for="check_out">Check Out</label>
                        <input type="date" name="check_out" id="check_out" required="Required" class="form-control" placeholder="Select suitable date">
                    </div>
                    {{-- <div class="form-group guest">
                        <label for="guest">Guest</label><br>
                        <label for="Adult">Adult</label>
                        <select name="adult" id="" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                        <label for="Children">Children</label>
                        <select name="children" id="" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <div class="form-group room">
                        <label for="Room">Room</label>
                            <select name="room" id="" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            </select>
            
                    </div> --}}
                <input type="hidden" id="room_total" value="{{ $rooms->price }}">
                <input type="hidden" name="room_quantity" id="room_quantity" value="{{ $rooms->quantity }}">
                {{-- <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}"> --}}
                <input type="hidden" id="room_id" name="room_id" value="{{ $rooms->id }}">
                <button class="moreDetailsBtn" id="bookingBtn" type="submit"> Booking now</button>
            </div>
        </div>
      
    </div>
    <script>
       
        // let checkin = new Date().toISOString().split('T')[0];
        // document.getElementsByName("check_in")[0].setAttribute('min', checkin);
        

        const checkInInput = document.getElementById('check_in');
        const checkOutInput = document.getElementById('check_out');
        const bookingBtn = document.getElementById('bookingBtn');
        const user_id = document.getElementById('user_id');
        const room_id = document.getElementById('room_id');
        const room_quantity = document.getElementById('room_quantity');
        const room_total = document.getElementById('room_total');


        let today = new Date();
        let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

        const todaysDate = Date.parse(date);

        //validate of check in date is valid;
 
        bookingBtn.addEventListener('click', function (){
        
        
        const checkInValue = checkInInput.value;
        const checkOutValue = checkOutInput.value;
        
        const checkIn = Date.parse(checkInValue);
        const checkOut = Date.parse(checkOutValue);
        
        console.log(checkInInput.innerHTML);
        
        if(checkInInput.value === "" || checkOutInput.value === ""){
            iziToast.warning({
                            title: 'For Reservation:',
                            message: 'Please input a date',
                            position: 'topRight',
                            transitionIn: 'fadeInUp',
                            transitionOut: 'fadeOut',
                            transitionInMobile: 'fadeInUp',
                            transitionOutMobile: 'fadeOutDown',
                        });
        }
        else if(checkIn < todaysDate){
            iziToast.warning({
                            title: '',
                            message: 'Wag mo na balikan ang kahapon',
                            position: 'topRight',
                            transitionIn: 'fadeInUp',
                            transitionOut: 'fadeOut',
                            transitionInMobile: 'fadeInUp',
                            transitionOutMobile: 'fadeOutDown',
                        });
            }
            else if(checkOut < checkIn){
                iziToast.error({
                            title: 'Error',
                            message: 'Invalid date, Please input a correct date',
                            position: 'topRight',
                            transitionIn: 'fadeInUp',
                            transitionOut: 'fadeOut',
                            transitionInMobile: 'fadeInUp',
                            transitionOutMobile: 'fadeOutDown',
                        });
            } 
            else {

                let data = new FormData;

                data.append('_token', "{{ csrf_token() }}");
                data.append('check_in', checkInInput.value);
                data.append('check_out', checkOutInput.value);
                data.append('room_id', room_id.value);
                // data.append('user_id', user_id.value);
                data.append('room_quantity', room_quantity.value);
                data.append('room_total', room_total.value);

                fetch('/book-room', {
                    method: 'post',
                    body: data
                }).then(function (response){
                    return response.text();
                }).then(function (data){
                    if(data === "not available"){
                    iziToast.error({
                        title: 'Room',
                        message: 'Room is not available, Check for another date',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });
                } else if(data === "book failed") {
                    iziToast.error({
                        title: '',
                        message: 'Room is Available, but please register or login to book a room',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });
                }
                else  {
                    var delay = 3000;

                    if(data === "available"){
                        iziToast.success({
                        title: 'Room',
                        message: 'Booking is successful, redirecting to your booking details',
                        position: 'topRight',
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOut',
                        transitionInMobile: 'fadeInUp',
                        transitionOutMobile: 'fadeOutDown',
                    });

                        setTimeout(function() {
                        window.location.replace('/show-users-booking');
                    }, delay);

                    }
                    
                }
                    checkInInput.value = "";
                    checkOutInput.value = "";

                });
            }
            
        }) 


    </script>
@endsection