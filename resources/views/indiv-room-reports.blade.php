@extends('layouts.templates.template')
@section('title', 'Reports')
@section('content')

    <div class="container">
        <div class="card card-cascade narrower mt-5">
            <!--Card image-->
            <div
              class="view view-cascade gradient-card-header young-passion-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-center align-items-center">
              <h3 class="white-text mx-3 p-3 text-uppercase">Reports</h3>  
            </div>
            <!--/Card image-->
            <div class="px-4">
                {{-- <div class="gradient-card-header p-0">{{ $booking_reports->links() }}</div> --}}
              <div class="table-wrapper">
                <!--Table-->
                <table class="table table-hover mb-1">
          
                  <!--Table head-->
                  <thead class="text-uppercase text-center">
                    <tr>
                      <th class="th-sm font-weight-bold">Booking ID</th>
                      <th class="th-sm font-weight-bold">User Name</th>
                      <th class="th-sm font-weight-bold">Room</th>
                      <th class="th-sm font-weight-bold">Date Created</th>
                    </tr>
                  </thead>
                  <!--Table head-->
          
                  <!--Table body-->
                  <tbody class="text-center">
                    @foreach ($booking_reports as $booking)
                    <tr>
                    <td>{{ $booking->created_at }}</td>
                    <td>{{ $booking->firstname }} {{ $booking->lastname}}</td>
                    <td>{{ $booking->name }}</td>
                    <td>{{ $booking->created_at }}</td>
                      
                    </tr>
                @endforeach
                  </tbody>
                  <!--Table body-->
                </table>
                <!--Table-->
              </div>
              <a href="/download-report-csv" class="col-sm-3 btn btn-elegant">Download CSV File</a>
            </div>
            
          </div>
          <!-- Table with panel -->
    </div>

@endsection