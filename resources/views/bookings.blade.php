@extends('layouts.templates.user-template')
@section('title', 'My Booking Details')
@section('content')

<script
    src="https://www.paypal.com/sdk/js?client-id=AY2LiSONnc31cRvdssl0xv_6Hn_9lf0OWenaro3JccL4IShlIckl1PMiIuEJPjJ6PHJHJfMedJEMhb1W"> 
    // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>

<div class="container">
    <h1 class="text-center mt-3 mb-2">My Booking details</h1>
<h2 class="text-center">Booking Number: <span class="text-danger"> {{ $booking->created_at->format('U') }}</span></h2>

<div class="user-booking-details">
    <div class="bookingdetails border border-secondary p-2 m-1">
            <h4 class="text-primary pl-1">User Details</h4>
            <p><strong>Firstname:</strong> {{ $booking->user->firstname }}</p>
            <p><strong>Lastname:</strong> {{ $booking->user->lastname }}</p>
            <p><strong>Email:</strong> {{ $booking->user->email }}</p>
            <p><strong>Booking Check-In Date:</strong> {{ $booking->check_in }} </p>
            <p><strong>Booking Check-Out Date:</strong> {{ $booking->check_out }} </p>
            <p><strong>Booking Price</strong> <span class="total">{{ $booking->total }}</span></p>
    </div>
    <hr>
    <div class="room-details border border-secondary p-2 m-1">
            <div id="roomDetails">
                <div>
                    <h4 class="text-primary pl-1">Room Details</h4>
                    <p><strong>Room Type:</strong> {{$booking->room->category->name}} </p>
                    <p><strong>Room Price:</strong> <span class="text-warning font-weight-bold">{{ $booking->room->price }} </span> </p>
                    <p><strong>Room Capacity:</strong> {{ $booking->room->capacity }}</p>
                    <p><strong>Room Description:</strong> {{ $booking->room->description }}</p>
                    <p><strong>Payment Status</strong> {{ $booking->payment->name }}</p>    
                </div>
                <div>
                    <p><strong>Room Image:</strong><br>
                    <img src="{{ asset($booking->room->imgPath) }}" alt="" srcset="" width="400px" height="250px">
                    </p>
                </div>
            </div>
    </div>
    <hr>
    <div class="admin-remarks border border-secondary p-2 m-1">
            <h4 class="text-primary pl-1">Admin Remarks</h4>
            <p><strong>Admin Remarks:</strong> {{ $booking->status->name }}</p>
            <p><strong>Message:</strong> {{ $booking->message }}</p>
    </div>
</div>
    

@if (Auth::user()->role_id === 1 && $booking->status_id === 1)
    <form action="/cancel-booking" method="post">
        @csrf
        @method('DELETE')
        <input type="hidden" name="cancel_booking" value="{{ $booking->id }}">
        <button type="submit" class="moreDetailsBtn" onclick="return confirm('Are you sure you want to cancel this booking?')">Cancel This Booking</button>
    </form>
@endif


@if ($booking->status_id === 2 && ($booking->payment_id === 1 || $booking->payment_id === 2))
    <div class="bookings-button">
    <button class="moreDetailsBtn" onclick="window.print()"> Print Invoice </button>
    <button class="moreDetailsBtn"> Send Email Copy </button>
    </div>
    @elseif ($booking->status_id === 2)

    <form action="/check-out" method="post" id="paymentForm">
    @csrf
    <input type="hidden" name="total" value="{{ $booking->room->price }}">    
    <input type="hidden" name="booking_id" value="{{ $booking->id }}">
    
    <input type="hidden" name="room_id" value="{{ $booking->room_id }}">
    <input type="hidden" name="payment_id" value="1">
    Choose your payment
        <button type="submit" class="moreDetailsBtn">
            <i class="fas fa-wallet"></i>
             Over the counter
        </button>  
        <div id="paypal-button-container"></div>
    </form>



@endif

</div>

<script>


    paypal.Buttons({
        createOrder: function(data, actions) {
        // This function sets up the details of the transaction, including the amount and line item details.
        return actions.order.create({
            purchase_units: [{
            amount: {
                value: '{{ $booking->total }}'
            }
            }]
        });
        },
        onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        // In this function, we want to submit the same form that we create for 
        // the cash payment.
        // But we need to edit the value of the payment_id to the id of Paypal
        // Before we submit the form.
        const checkoutForm = document.getElementById('paymentForm');

        checkoutForm.lastElementChild.previousElementSibling.previousElementSibling.value = 2;
        console.log(checkoutForm);
        
        checkoutForm.submit();
      });
    }
    }).render('#paypal-button-container');

</script>

@endsection