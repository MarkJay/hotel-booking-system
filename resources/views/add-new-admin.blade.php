@extends('layouts.templates.template')
@section('title', 'Add New Admin')
@section('content')

    <h1 class="text-center mt-5">Add new Admin</h1>

    <form action="/admin-user" method="POST" class="mt-5 d-flex flex-column justify-content-center align-items-center col-lg-12">
    @csrf
        <div class="form-group">
            <label for="">Firstname</label>
            <input type="text" name="firstname" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Lastname</label>
            <input type="text" name="lastname" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="text" name="password" class="form-control">
        </div>
        
        <button type="submit" class="btn btn-unique">Submit</button>
    </form>


@endsection