@extends('layouts.templates.template')
@section('title', 'All Categories')
@section('content')

<div class="container">
<div class="card card-cascade narrower mt-5">
    <!--Card image-->
    <div
      class="view view-cascade gradient-card-header young-passion-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-center  align-items-center">
      <h3 class="white-text mx-3 p-3 text-uppercase"> Room Facilities</h3>  
    </div>
    <!--/Card image-->
    
    <div class="px-4">
        <div class="gradient-card-header">{{ $facilities->links() }}</div>
      <div class="table-wrapper">
        <!--Table-->
        <table class="table table-hover mb-1">
  
          <!--Table head-->
          <thead class="text-uppercase">
            <tr>
              <th class="th-lg font-weight-bold">
                Facilities Name
              </th>
              <th class="th-lg font-weight-bold">
                <a href="">Date Created
                </a>
              </th>
              <th class="th-lg font-weight-bold ">
                <a href="">Date Updated
                  
                </a>
              </th>
              <th class="th-lg font-weight-bold">
                <a href="">Action
                  
                </a>
            </tr>
          </thead>
          <!--Table head-->
  
          <!--Table body-->
          <tbody class="text-center">
            @foreach ($facilities as $facility)

            <tr>
              <td>{{ $facility->name }}</td>
              <td>{{ $facility->created_at }}</span></td>
            
              <td><span class="bg-elegant">{{ $facility->updated_at }}</span></td>
              <td>
                <form action="/delete-facilities" method="post">
                @csrf
                @method('DELETE')
                @foreach ($facility->room as $room)
                <input type="hidden" name="room_id" value="{{ $room->pivot->room_id }}">
                @endforeach
                <input type="hidden" name="facility_id" value="{{ $facility->id }}">
                    <button type="submit" class="btn btn-elegant" onclick="return confirm('Are you sure you want to delete this item?')"><i class="fas fa-trash"></i> Delete</button>
                <a href="/update-facilities/{{ $facility->id }}" class="btn btn-ripe-malinka-gradient"><i class="fas fa-edit"></i> Update</a>
                </form>
            </td>
            </tr>
        @endforeach
          </tbody>
          <!--Table body-->
        </table>
        <!--Table-->
      </div>
  
    </div>
  
  </div>
</div>
   <script>
       const deleteBtns = document.querySelectorAll('.deleteBtn');
    console.log(deleteBtns);

       deleteBtns.forEach(function(deleteBtn){
            deleteBtn.addEventListener('click', function(){
            
            })
       })

   </script>
@endsection