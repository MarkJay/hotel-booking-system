<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
   

    public function category(){
        return $this->belongsTo('App\Category');
    }

    // public function booking(){
    //     return $this->hasMany('App\Booking', 'room_id', 'id');
    // }

    public function facility(){
        return $this->belongsToMany('App\Facilty')->withTimeStamps();
    }

    public function booking(){
        return $this->belongsToMany('App\Room')->withTimeStamps();
    }
}
