<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

 

  public function room(){
      return $this->belongsTo('App\Room', 'room_id', 'id');
  }

  public function status(){
      return $this->belongsTo('App\Status', 'status_id', 'id');
  }

  public function user(){
    return $this->belongsTo('App\User', 'user_id', 'id');
  }

  public function payment(){
    return $this->belongsTo('App\Payment', 'payment_id', 'id');
  }

  
 
}
