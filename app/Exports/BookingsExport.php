<?php

namespace App\Exports;

use App\Booking;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class BookingsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection

    */

   public function view(): View {
    
    $room = request()->session()->get('date');
    $from_date = request()->session()->get('date');
    $to_date = request()->session()->get('date');

    $room_category = $room['room_category'];
    $from = $from_date['from'];
    $to = $to_date['to'];

        
        if($room_category == ""){

            $bookings = Booking::whereBetween('created_at', [$from, $to])->paginate(10);
            return view('booking-reports', compact('bookings'));
        
        }
        else {

            $bookings = DB::table('bookings')
                ->join('rooms', 'rooms.id', '=', 'bookings.room_id')
                ->join('users', 'users.id', '=', 'bookings.user_id')
                ->join('payments', 'payments.id', '=', 'bookings.payment_id')
                ->join('categories', 'categories.id', '=', 'rooms.category_id')
                ->whereBetween('bookings.created_at', [$from, $to])
                ->where('categories.id', $room_category)
                ->get();

                return view('booking-indiv-reports', compact('bookings'));
           
        }
    } 
       
   }



