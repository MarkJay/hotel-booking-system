<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;


class ContactController extends Controller
{
    public function contactUs(){

        return view('contact-us');
    }

    public function sendMessage(Request $request){

        $contact = new Contact;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->save();

        return "success";
    }

    public function showAllMessages(){

        $messages = Contact::orderBy('created_at')->paginate(10);

        return view('all-messages', compact('messages'));

    }

    public function showMessage($id){

        $message = Contact::find($id);

        return view('show-message', compact('message'));

    }

    public function deleteMessage(Request $request){

        $id = $request->message_id;
        $message = Contact::find($id);
        $message->delete();

        return redirect('/messages');

    }
}
