<?php

namespace App\Http\Controllers;

use App\About;
use App\Facilty;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function aboutUs(){

       $aboutpage = About::all();

       return view('about-us-page-admin', compact('aboutpage'));
    }

    public function aboutUsUser(){

        $aboutpage = About::all();
        $facilities = Facilty::orderBy('created_at')->take(4)->get();

        return view('about-us', compact('aboutpage', 'facilities'));

    }

    public function create(){

        return view('create-about-us-page');
    }

    public function store(Request $request){

        $rules = array (
        
            'description' => 'required',
            'imgPath' => 'required|image|mimes:jpeg,jpg,png,svg,gif,JPEG,JPG,PNG,SVG,GIF'
    
    );

        $this->validate($request, $rules);

        $aboutpage = new About;
        $aboutpage->description = $request->description;
        
        $img = $request->file('imgPath');

        $image_name = time() . "." . $img->getClientOriginalExtension();

        $destination = "images/";

        $img->move($destination, $image_name);

        // $aboutpage->imgPath = $destination . $image_name;
        $result = \Cloudinary\Uploader::upload($destination.$image_name);

        $aboutpage->imgPath = $result['secure_url'];

        $aboutpage->save();

        return redirect('/about-us-page');

    }

    public function destroy(Request $request){

        $id = $request->aboutpage_id;
        $about = About::find($id);
        $about->delete();

        return redirect('/about-us-page');

    }

    public function edit($id){

        $aboutpage = About::find($id);

        return view('update-about-us-page', compact('aboutpage'));
    }

    public function update($id, Request $request){


        $rules = array (
        
            'description' => 'required',
            'imgPath' => 'required|image|mimes:jpeg,jpg,png,svg,gif,JPEG,JPG,PNG,SVG,GIF'
    
    );

        $this->validate($request, $rules);

        $aboutpage = About::find($id);
        $aboutpage->description = $request->description;

        $img = $request->file('imgPath');

        $image_name = time() . "." . $img->getClientOriginalExtension();

        $destination = "images/";

        $img->move($destination, $image_name);

        // $aboutpage->imgPath = $destination . $image_name;
        $result = \Cloudinary\Uploader::upload($destination.$image_name);

        $aboutpage->imgPath = $result['secure_url'];

        $aboutpage->save();

        return redirect('/about-us-page');

    }
    
}
