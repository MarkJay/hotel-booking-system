<?php

namespace App\Http\Controllers;

use Auth;
use App\Room;
use App\Booking;
use App\Status;
use App\Category;
use App\Payment;
use Carbon\Carbon;
use DateTime;
use DB;

use Illuminate\Http\Request;

class BookingController extends Controller
{
    
    public function bookRoom(Request $request){

        
    $check_in = $request->check_in;
    $check_out = $request->check_out;
    $room_quantity = $request->room_quantity;
    $room_id = $request->room_id;
    

    
    $datetime_first = new DateTime($check_in);
    $datetime_second = new DateTime($check_out);
    $interval = $datetime_first->diff($datetime_second);
    $total_days = $interval->format('%a');

    $room_total = $request->room_total * $total_days;

    $rangeCount = Booking::where('room_id', $request->room_id)
    ->where('status_id', '!=', 3)
    ->where(function ($query) use ($check_in, $check_out) {
     $query->where(function ($q) use ($check_in, $check_out) {
         $q->where('check_in', '>=', $check_in)
            ->where('check_in', '<=', $check_out);
     })->orWhere(function ($q) use ($check_in, $check_out) {
         $q->where('check_in', '<=', $check_in)
            ->where('check_out', '>=', $check_out);
     })->orWhere(function ($q) use ($check_in, $check_out) {
         $q->where('check_out', '>=', $check_in)
            ->where('check_out', '<=', $check_out);
     })->orWhere(function ($q) use ($check_in, $check_out) {
         $q->where('check_in', '>=', $check_in)
            ->where('check_out', '<=', $check_out);
     });
 })->count();


        if($rangeCount >= $room_quantity){

            return "not available";

        } else {

            if(!Auth::user()){

                return "book failed";
            }
            else {

            $new_booking = new Booking;

            $new_booking->check_in = $request->check_in;
            $new_booking->check_out = $request->check_out;
            $new_booking->user_id = Auth::user()->id;
            $new_booking->room_id = $request->room_id;
            $new_booking->total = $room_total;
            $new_booking->save();

            return "available";

            }

        }
 
    }

    public function showAllBooking(){    
        $bookings = Booking::orderBy('created_at', 'asc')->paginate(7);
        $rooms = Room::all();

        return view('all-bookings', compact('rooms', 'bookings'));
    }

    public function showBookingDetails($id){

        $booking = Booking::find($id);
        $statuses = Status::all();

        return view('show-booking-details', compact('booking', 'statuses'));
    }

    public function showNewBooking(){

        $bookings = Booking::where('status_id', '1')->paginate(5);
        

        return view('new-bookings', compact('bookings'));
    }

    public function showApprovedBooking(){
        $bookings = Booking::where('status_id', '2')->paginate(5);

        return view('approved-bookings', compact('bookings'));
    }

    public function showCancelledBooking(){
        $bookings = Booking::where('status_id', '3')->paginate(5);

        return view('cancelled-bookings', compact('bookings'));
    }

    public function showUserBooking(){


        $userBooking = Booking::where('user_id', Auth::user()->id)->get();

        return view('show-user-selected-booking', compact('userBooking'));

        
    }

    public function updateBooking(Request $request){

        $id = $request->booking_id;
        $booking = Booking::find($id);
        $booking->message = $request->admin_remarks;
        $booking->status_id = $request->status_id;
        $booking->save();

        return "success";
    }

    public function showAllBookingDetails($id){
        $booking = Booking::find($id);
        $statuses = Status::all();
        return view('show-all-booking-details', compact('booking', 'statuses'));
    }

    public function userHome(){

        $rooms_categories = Category::all();
        $rooms = Room::orderBy('price', 'asc')->take(4)->get();
        
        return view('users-home', compact('rooms_categories', 'rooms'));

    }

    public function findCategoryName(Request $request){
        
        $data = Room::select('quantity', 'capacity', 'category_id', 'id')->where('category_id', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function checkRoomAvailability(Request $request){

    $check_in = $request->check_in;
    $check_out = $request->check_out;
    $room_id = $request->room_id;
    $room_capacity = $request->room_capacity;
    $room_quantity = $request->room_quantity;

    // $room_quantity = DB::table('bookings')
    // ->join('rooms', 'rooms.id', '=', 'bookings.room_id')
    // ->select('rooms.quantity')
    // ->where('rooms.category_id', $room_id)  
    // ->first();
    

    // $room_capacity = DB::table('bookings')
    // ->join('rooms', 'rooms.id', '=', 'bookings.room_id')
    // ->select('rooms.capacity')
    // ->where('rooms.id', $request->room_id);
    
    
    $rangeCount = Booking::where('room_id', $request->room_id)
    ->where(function ($query) use ($check_in, $check_out) {
        $query->where(function ($q) use ($check_in, $check_out) {
            $q->where('check_in', '>=', $check_in)
                ->where('check_in', '<=', $check_out);
        })->orWhere(function ($q) use ($check_in, $check_out) {
            $q->where('check_in', '<=', $check_in)
                ->where('check_out', '>=', $check_out);
        })->orWhere(function ($q) use ($check_in, $check_out) {
            $q->where('check_out', '>=', $check_in)
                ->where('check_out', '<=', $check_out);
        })->orWhere(function ($q) use ($check_in, $check_out) {
            $q->where('check_in', '>=', $check_in)
                ->where('check_out', '<=', $check_out);
        });
    })->count();

    
    
        if($rangeCount > 1){

            return "not available";
        } else {
            
            return "available";
            
        }
     
    }


    public function showUserBookingDetails($id){
        
        $booking = Booking::find($id);
        $statuses = Status::all();

        return view('bookings', compact('booking', 'statuses'));
    }

    public function cancelBooking(Request $request){

        $id = $request->cancel_booking;
        $booking = Booking::find($id);
        $booking->delete();

        return redirect('/show-users-booking');

    }

    public function checkout(Request $request){

        $booking_id = $request->booking_id;
        
        // $room_quantity = DB::table('bookings')
        // ->join('rooms', 'rooms.id', '=', 'bookings.room_id')
        // ->select('rooms.quantity')
        // ->where('rooms.id', $request->room_id)
        // ->get();


        $updateBooking = Booking::find($booking_id);
        $updateBooking->payment_id = $request->payment_id;
        $updateBooking->save();

        return redirect('/user-booking-details/' . $booking_id);
    }


    // Reports Section

    public function reports(){

        $categories = Category::all();

        return view('generate-reports', compact('categories'));
    }

    public function generateReports(Request $request){

       
        $room_category = $request->room_category;
        $from = $request->from;
        $to = $request->to;

        $request->session()->put('date', $request->input());

        if($room_category == ""){

            $booking_reports = Booking::whereBetween('created_at', [$from, $to])->paginate(10);
           
            return view('reports', compact('booking_reports'));

        }
        else {

            $booking_reports = DB::table('bookings')
                ->join('rooms', 'rooms.id', '=', 'bookings.room_id')
                ->join('users', 'users.id', '=', 'bookings.user_id')
                ->join('payments', 'payments.id', '=', 'bookings.payment_id')
                ->join('categories', 'categories.id', '=', 'rooms.category_id')
                ->whereBetween('bookings.created_at', [$from, $to])
                ->where('categories.id', $room_category)
                ->get();

               
                return view('indiv-room-reports', compact('booking_reports'));
           
        }
        

        
    }

    public function search(){

        return view('search');
    }

}
