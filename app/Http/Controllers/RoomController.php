<?php

namespace App\Http\Controllers;

use App\Category;
use App\Room;
use App\User;
use App\Facilty;
use Auth;
use Illuminate\Http\Request;

class RoomController extends Controller
{

    public function index(){

        $rooms = Room::orderBy('updated_at', 'desc')->paginate(5);
        $category = Category::all();

        return view('rooms', compact('rooms', 'category'));
    }

    public function create(){

        $categories = Category::all();

        return view('add-rooms', compact('categories'));
    }

    //Adding new Rooms
    public function store(Request $request){

        $rules = array (
            'category_id' => 'required|unique:rooms',
            'room_description' => 'required',
            'room_quantity' => 'required',
            'room_bed' => 'required',
            'room_capacity' => 'required',
            'room_price' => 'required',
            'imgPath' => 'required|image|mimes:jpeg,jpg,png,svg,gif,JPEG,JPG,PNG,SVG,GIF'
        );

        $this->validate($request, $rules);

        $room = new Room;

        $room->category_id = $request->category_id;
        $room->description = $request->room_description;
        $room->quantity = $request->room_quantity;
        $room->bed_room = $request->room_bed;
        $room->capacity = $request->room_capacity;
        $room->price = $request->room_price;
        
        $img = $request->file('imgPath');

        $image_name = time() . "." . $img->getClientOriginalExtension();

        $destination = "images/";

        $img->move($destination, $image_name);

        // $room->imgPath = $destination . $image_name;

        $result = \Cloudinary\Uploader::upload($destination.$image_name);

        $room->imgPath = $result['secure_url'];
        
        $room->save();

        return redirect('/rooms');
    }
    // Delete Rooms
    public function destroy(Request $request){
        $facility_id = $request->facility_id;
        $id = $request->room_id;
        $room = Room::find($id);

        $room->facility()->detach([$facility_id]);

        

        $room->delete();

        return redirect('/rooms');
    }
    //View edit Form
    public function edit($id){

        $categories = Category::all();
        $rooms = Room::find($id);

        return view('update-rooms', compact('rooms', 'categories'));
    }

    //Update Rooms

    public function update($id, Request $request){
        
        $rules = array (
            'category_id' => 'required',
            'room_description' => 'required',
            'room_quantity' => 'required',
            'room_bed' => 'required',
            'room_capacity' => 'required',
            'room_price' => 'required',
            'imgPath' => 'required|image|mimes:jpeg,jpg,png,svg,gif,JPEG,JPG,PNG,SVG,GIF'
        );

        $this->validate($request, $rules);

        $room = Room::find($id);
        $room->category_id = $request->category_id;
        $room->description = $request->room_description;
        $room->quantity = $request->room_quantity;
        $room->bed_room = $request->room_bed;
        $room->capacity = $request->room_capacity;
        $room->price = $request->room_price;

        $img = $request->file('imgPath');

        $image_name = time() . "." . $img->getClientOriginalExtension();

        $destination = "images/";

        $img->move($destination, $image_name);

        // $room->imgPath = $destination . $image_name;
        
        $result = \Cloudinary\Uploader::upload($destination.$image_name);

        $room->imgPath = $result['secure_url'];

        $room->save();

        return redirect('/rooms');
    }

    // Show All Rooms for user

    public function showRooms(){

        $rooms = Room::orderBy('category_id', 'asc')->paginate(6);
        $categories = Category::all();
        $facility = Facilty::all();
        
        return view('show-rooms', compact('rooms', 'categories', 'facility'));
    }
    // Selected Room View

    public function showSelectedRoom($id){
        $rooms = Room::find($id);
        $category = Category::all();
        $facility = Facilty::all();
        return view('show-selected-room', compact('rooms', 'category', 'facility'));
    }

    

}
