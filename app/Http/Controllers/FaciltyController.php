<?php

namespace App\Http\Controllers;

use App\Room;
use App\Facilty;
use App\Category;
use Illuminate\Http\Request;

class FaciltyController extends Controller
{
    public function index(){
        
        $facilities = Facilty::orderBy('created_at', 'desc')->paginate(5);

        return view('facilities', compact('facilities'));
    }

    public function create(){
        return view('add-facilities');
    }
    public function store(Request $request){
        
        $facility = new Facilty;
        $facility->name = $request->facility_name;
        $facility->save();

        return "success";
    }

    public function destroy(Request $request){

        $id = $request->facility_id;
        $room_id = $request->room_id;
        $facility = Facilty::find($id);

        $facility->room()->detach($room_id);
        
        $facility->delete();
        
        return redirect('/facilities');
    }

    public function edit($id){

        $facility = Facilty::find($id);

        return view('update-facilities', compact('facility'));
    }

    public function update($id, Request $request){
        
        $facility = Facilty::find($id);

        $facility->name = $request->facility_name;
        $facility->save();
        
        return "success";

    }

    public function addToRoomFacility(){

        $facilities = Facilty::all();
        $rooms = [];
        return view('add-facility-room', compact('facilities', 'rooms'));
    }

    public function createRoomFacility(Request $request){

         
        $room_id = $request->room_id;
        $facility_id = $request->facility_id;
        $room = Room::find($room_id);

        $hasFacility = $room->facility()->where('facilty_id', $facility_id)->exists();
        
        if($hasFacility === false){

            $room->facility()->attach($facility_id);
            // $rooms = $room->facility->toArray();
            return "success";
        } else {
            return "duplicate";
        }
            
    }

    public function removeFacilityRoom(Request $request){

        $room_id = $request->room_id;
        $facility_id = $request->facility_id;

        $room = Room::find($room_id);
        $room->facility()->detach($facility_id);

        return back();
    }

    // public function searchRoom(Request $request){

    //     $search = $request->get('search_room');
    //     $room_facilities = Category::where('name', 'like', '%' .$search. '%')->get();
        
    //     return view('add-facility-room', compact('room_facilities'));
    // }

}
