<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function show(){

        $users = User::where('role_id', 1)->paginate(6);

        return view('all-user-registered', compact('users'));
    }

    public function create(){

        return view('add-new-admin');
    }

    public function showAdmin(){
        $users = User::where('role_id', 2)->paginate(6);

        return view('all-admin', compact('users'));
    }

    public function store(Request $request){

        $admin = new User;

        $admin->firstname = $request->firstname;
        $admin->lastname = $request->lastname;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $admin->role_id = 2;
        $admin->save();


        return redirect('/all-admin');
    }

    public function destroy(Request $request){

        $id = $request->admin_id;
        $user = User::find($id);
        $user->delete();

        return redirect('/admin-user');
    }

    public function deleteUser(Request $request){

        $id = $request->user_id;
        $user = User::find($id);
        $user->delete();

        return redirect('/all-user-registered');
    }

}
