<?php

namespace App\Http\Controllers;

use App\Profile;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){

        $profiles = Profile::where('user_id', Auth::user()->id)->get();

        return view('user-profile', compact('profiles'));

    }

    public function create(){
        return view('create-user-profile');
    }

    public function store(Request $request){

        $rules = array (
            'address' => 'required',
            'contact_number' => 'required',
            'imgPath' => 'required|image|mimes:jpeg,jpg,png,svg,gif,JPEG,JPG,PNG,SVG,GIF'
        );

        $this->validate($request, $rules);

        $profile = new Profile;
        $profile->address = $request->address;
        $profile->number = $request->contact_number;
        $profile->user_id = Auth::user()->id;
        
        $img = $request->file('imgPath');

        $image_name = time() . "." . $img->getClientOriginalExtension();

        $destination = "images/";

        $img->move($destination, $image_name);

        $profile->imgPath = $destination . $image_name;

        $profile->save();


        return redirect('/user-profile');

    }   

    public function destroy(Request $request){
        $id = $request->profile_id;
        $profile = Profile::find($id);

        $profile->delete();

        return redirect('/user-profile');
    }

    public function edit($id){

        $profile = Profile::find($id);
        
        return view('update-user-profile', compact('profile'));

    }

    public function update($id, Request $request){

        $rules = array (
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'address' => 'required',
            'contact_number' => 'required',
            'imgPath' => 'required|image|mimes:jpeg,jpg,png,svg,gif,JPEG,JPG,PNG,SVG,GIF'
        );
        
        $this->validate($request, $rules);

        $profile = Profile::with('user')->findOrFail($id);
        
        $profile->user->firstname = $request->firstname;
        $profile->user->lastname = $request->lastname;
        $profile->user->email = $request->email;
        $profile->address = $request->address;
        $profile->number = $request->contact_number;
        
        $img = $request->file('imgPath');

        $image_name = time() . "." . $img->getClientOriginalExtension();

        $destination = "images/";

        $img->move($destination, $image_name);

        $profile->imgPath = $destination . $image_name;

        $profile->save();
        $profile->user->save();


        return redirect('/user-profile');
    }
}
