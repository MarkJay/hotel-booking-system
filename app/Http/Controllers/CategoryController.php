<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){

        $categories = Category::orderBy('created_at', 'desc')->paginate(5);

        return view('categories', compact('categories'));
    }
    public function create(){
        return view('add-categories');
    }
    public function store(Request $request){
        $rules = array (
            'room_category' => 'required'
           
        );

        $this->validate($request, $rules);

        $room_name = $request->room_category;
        $checkRoomDuplicate = Category::where('name', '=', $room_name)->first();

        if($checkRoomDuplicate === null) {

            $category = new Category;
            $category->name = $request->room_category;
            $category->save();

            return "success";
            
        }
        else {
            return "duplicate";
        }
        
        
    }

    public function destroy(Request $request){

        $id = $request->category_id;
        $category = Category::find($id);
        $category->delete();

        return redirect('/categories');
    }

    public function edit($id){

        $category = Category::find($id);
        
        return view('update-categories', compact('category'));
    }

    public function update($id, Request $request){
        
        $rules = array (
            'room_category' => 'required'
            

        );

        $this->validate($request, $rules);

        $category = Category::find($id);
        $category->name = $request->room_category;
        $category->save();

        return "success";
    }

}
